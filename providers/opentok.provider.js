const OpenTokLib = require('opentok');

// Config
const config = require('../config/config');

const opentokContainer = {};

opentokContainer.generateSession = () => {
    return new Promise((resolve, reject) => {
        const opentok = new OpenTokLib(config.tokenBox.apiKey, config.tokenBox.apiSecret);
        opentok.createSession({mediaMode: "routed"}, (err, session) => {
            if (err) {
                return reject({
                    message: err,
                    bool: false
                });
            }
            resolve(session.sessionId);
        });
    });
};

opentokContainer.generateToken = sessionID => {
    const opentok = new OpenTokLib(config.tokenBox.apiKey, config.tokenBox.apiSecret);
    return opentok.generateToken(sessionID, {
        role: 'publisher'
    });
};

module.exports = opentokContainer;