const UserModel = require('../models/user');
const PatientModel = require('../models/patient');
const HealthModel = require('../models/patient');
const obj = {};
const getPatientProfile = userID => {
    return new Promise(async (resolve, reject) => {
        try {
            let patient = await PatientModel.getByUserID(userID);
            if (patient.firstname === '' || patient.lastname === '' || patient.user_email === '' || patient.city === '' || patient.country === '' || patient.gender === '' || patient.timezone === '' || patient.phone_mobile === '' || patient.height_ft === '' || patient.height_inch === '' || patient.weight === '') {
                return resolve(false);
            }
            resolve(patient);
        } catch (e) {
            reject(e);
        }
    });
};
const getHealthProfile = (userID, type) => {
    return new Promise(async (resolve, reject) => {
        try {
            const healthData = await HealthModel.getByUserID(userID);
            if (!healthData) {
                return resolve(false);
            }
            resolve(healthData);
        } catch (e) {
            reject(e);
        }
    });
};
obj.getProfileCompletion = userID => {
    return new Promise(async (resolve, reject) => {
        try {
            if (!userID) {
                return reject({
                    message: 'Please provide User ID.',
                    bool: false
                });
            }
            const returnObj = {};
            returnObj.percentage = 0;
            returnObj.text = '';
            returnObj.type = '';

            const patient = await getPatientProfile(userID);
            if (!patient) {
                returnObj.percentage = 25;
                returnObj.text = 'Please complete your Profile';
                returnObj.type = 'Profile';
                return resolve(returnObj);
            }
            const health = await getHealthProfile(userID, 'health');
            if(!patient.is_health || (patient.is_health === 2 && !health)){
                returnObj.percentage = 60;
                returnObj.text = 'Please complete My Health History';
                returnObj.type = 'Health';
                return resolve(returnObj);
            }
            const medication = await getHealthProfile(userID, 'medication');
            if(!patient.is_medication || (patient.is_medication === 2 && !medication)){
                returnObj.percentage = 70;
                returnObj.text = 'Please complete My Health Medication';
                returnObj.type = 'Medication';
                return resolve(returnObj);
            }
            const allergies = await getHealthProfile(userID, 'allergies');
            if(!patient.is_allergies || (patient.is_allergies === 2 && !allergies)){
                returnObj.percentage = 80;
                returnObj.text = 'Please complete My Health Allergies';
                returnObj.type = 'Allergies';
                return resolve(returnObj);
            }
            const surgery = await getHealthProfile(userID, 'surgery');
            if(!patient.is_surgeries || (patient.is_surgeries === 2 && !surgery)){
                returnObj.percentage = 90;
                returnObj.text = 'Please complete My Health Surgery';
                returnObj.type = 'Surgeries';
                return resolve(returnObj);
            }
            returnObj.percentage = 100;
            returnObj.text = 'Your profile is 100% complete';
            returnObj.type = 'Complete';
            return resolve(returnObj);
        } catch (e) {
            reject(e);
        }
    });
};

module.exports = obj;