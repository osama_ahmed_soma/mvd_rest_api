const request = require('request');
const obj = {};
const getGPI10 = keyword => {
    return new Promise(async (resolve, reject) => {
        const options = {
            method: 'GET',
            url: 'http://c.qa.med.api.rxcut.com/1.1/GetGPI10s',
            qs: {
                search: keyword,
                uid: 'C663A703-D98D-4D8D-875D-F85EF1B9779C'
            },
            headers: {
                'cache-control': 'no-cache'
            }
        };
        request(options, (error, response, body) => {
            if (error) {
                return reject({
                    message: error,
                    bool: false
                });
            }
            body = JSON.parse(body);
            if(body.hasOwnProperty('status') && body.status === 'error'){
                return reject({
                    message: (body.hasOwnProperty('error') && body.error !== '') ? body.error : 'No data return from api. There is an error.',
                    bool: false
                });
            }
            returnData = [];
            if (body.drugs.length > 0) {
                for (let i = 0; i < body.drugs.length; i++) {
                    const GPI10s = body.drugs[i].GPI10s;
                    for (let x = 0; x < GPI10s.length; x++) {
                        returnData.push(GPI10s[x]);
                        if (i == (body.drugs.length - 1) && x == (GPI10s.length - 1)) {
                            resolve(returnData);
                        }
                    }
                }
            } else {
                resolve(returnData);
            }
        });
    });
};

const isFreqRankInArray = (FreqRank, drugs) => {
    return new Promise(resolve => {
        let inArray = false;
        for (let x = 0; x < drugs.length; x++) {
            if (FreqRank == drugs[x].FreqRank) {
                inArray = true;
            }
            if (x == (drugs.length - 1)) {
                resolve(inArray);
            }
        }
    });
};

const getGPI14 = GPI10s => {
    return new Promise(async (resolve, reject) => {
        const options = {
            method: 'GET',
            url: 'http://c.qa.med.api.rxcut.com/1.1/GetGPI14s',
            qs: {
                GPI10: GPI10s,
                uid: 'C663A703-D98D-4D8D-875D-F85EF1B9779C',
                freqrank: true,
                prediction: 100000
            },
            headers: {
                'cache-control': 'no-cache'
            }
        };
        request(options, async (error, response, body) => {
            if (error) {
                return reject({
                    message: error,
                    bool: false
                });
            }
            body = JSON.parse(body);
            const returnData = {
                drugs: []
            };
            if (body.hasOwnProperty('predictions')) {
                const predictions = body.predictions;
                for (let i = 0; i < predictions.length; i++) {
                    if (returnData.drugs.length > 0) {
                        const inArray = await isFreqRankInArray(predictions[i].FreqRank, returnData.drugs);
                        if (!inArray) {
                            returnData.drugs.push(predictions[i]);
                        }
                    } else {
                        returnData.drugs.push(predictions[i]);
                    }
                    if (i == (predictions.length - 1)) {
                        resolve(returnData);
                    }
                }
            } else {
                resolve(returnData);
            }
        });
    });
};

const checkDuplicateNames = (LN, names) => {
    return new Promise(resolve => {
        let inArray = false;
        if (names.length > 0) {
            for (let x = 0; x < names.length; x++) {
                if (LN == names[x]) {
                    inArray = true;
                }
                if (x == (names.length - 1)) {
                    return resolve(inArray);
                }
            }
        } else {
            return resolve(inArray);
        }
    });
};

obj.getMedicine_api = keyword => {
    return new Promise(async (resolve, reject) => {
        try {
            const gpi10Data = await getGPI10(keyword);
            const object = {
                full_data: {},
                full_data_array: [],
                names: []
            };
            if (gpi10Data.length > 0) {
                const gpi14Data = await getGPI14(gpi10Data.join());
                const drugs = gpi14Data.drugs;
                let returnData = [];
                for (let i = 0; i < drugs.length; i++) {
                    if ((drugs[i].DosageForm == 'LIQUID' || drugs[i].DosageForm == 'TABLET') && drugs[i].Strength != '') {
                        returnData.push(drugs[i]);
                    }
                    if (i == (drugs.length - 1)) {
                        const object = {
                            full_data: {},
                            full_data_array: [],
                            names: []
                        };
                        for (let x = 0; x < returnData.length; x++) {
                            const inArray = await checkDuplicateNames(returnData[x].LN, object.names);
                            if (!inArray) {
                                object.full_data[returnData[x].LN] = returnData[x];
                                object.full_data_array.push(returnData[x]);
                                object.names.push(returnData[x].LN);
                            }
                            if (x == (returnData.length - 1)) {
                                return resolve(object);
                            }
                        }
                    }
                }
            } else {
                return resolve(object);
            }
        } catch (e) {
            reject(e);
        }
    });
};

module.exports = obj;