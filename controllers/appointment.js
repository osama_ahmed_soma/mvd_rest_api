const moment = require('moment-timezone');

// Models
const AppointmentModel = require('../models/appointment');
const AppointmentLogModel = require('../models/appointment_log');
const PatientModel = require('../models/patient');
const AppointmentKnockingDataModel = require('../models/appointment_knocking_data');

// Emitters
const Emitters = require('../emitters/emitter');

const appointmentCtrl = {};

const checkAndGetUpcoming = (userID) => {
    return new Promise(async (resolve, reject) => {
        try {
            // get patient data
            const patientData = await PatientModel.getByUserID(userID, 'timezone');
            const timezone = patientData.timezone;


            const currentTime = moment().tz(timezone).unix();
            const comingTime = moment.tz((currentTime * 1000), timezone).add(5, 'm').unix();
            let upcomingAppointment = await AppointmentModel.getByTimeAndPatientIDUpcoming(userID, currentTime, comingTime);
            let currentAppointment = await AppointmentModel.getByTimeAndPatientIDCurrent(userID, currentTime);
            let secondsLeft = 0;
            let isAction = false;
            let actionType = '';
            let canEmit = false;
            let appointmentID = 0;
            if (upcomingAppointment.length > 0) {
                canEmit = true;
                upcomingAppointment = upcomingAppointment[0];
                appointmentID = upcomingAppointment.id;
                // let upcomingAppointmentLogData = await AppointmentLogModel.getByAppointmentIDNotCancelled(appointmentID);
                let upcomingAppointmentLogData = await AppointmentLogModel.getByAppointmentID(appointmentID);
                if (upcomingAppointmentLogData.length > 0) {
                    upcomingAppointmentLogData = upcomingAppointmentLogData[0];
                    isAction = true;
                    actionType = upcomingAppointmentLogData.status;
                    switch (upcomingAppointmentLogData.status) {
                        case 'delayed':
                            secondsLeft = upcomingAppointmentLogData.stamp - currentTime;
                            break;
                        case 'accepted':
                            secondsLeft = upcomingAppointment.start_date_stamp - currentTime;
                            break;
                    }
                } else {
                    secondsLeft = upcomingAppointment.start_date_stamp - currentTime;
                }
            } else if (currentAppointment.length > 0) {
                canEmit = true;
                currentAppointment = currentAppointment[0];
                appointmentID = currentAppointment.id;
                // let currentAppointmentLogData = await AppointmentLogModel.getByAppointmentIDNotCancelled(appointmentID);
                let currentAppointmentLogData = await AppointmentLogModel.getByAppointmentID(appointmentID);
                if (currentAppointmentLogData.length > 0) {
                    currentAppointmentLogData = currentAppointmentLogData[0];
                    isAction = true;
                    actionType = currentAppointmentLogData.status;
                    switch (currentAppointmentLogData.status) {
                        case 'delayed':
                            secondsLeft = currentAppointmentLogData.stamp - currentTime;
                            break;
                        case 'accepted':
                            secondsLeft = currentAppointment.start_date_stamp - currentTime;
                            break;
                    }
                } else {
                    secondsLeft = currentAppointment.start_date_stamp - currentTime;
                }
            } else {
                // appointment log model
                // get appointment accepted Data with time
                let appointmentLogDelayedData = await AppointmentLogModel.getDelayedByUserIDAndTime(userID, moment(comingTime).add(10, 'minutes').format('YYYY-MM-DD HH:mm:ss'), currentTime);
                let appointmentLogAcceptedData = await AppointmentLogModel.getAcceptedByUserIDAndTime(userID, comingTime, currentTime);
                let appointmentLogCancelledData = await AppointmentLogModel.getCancelledByUserIDAndTime(userID, comingTime, currentTime);

                if (appointmentLogDelayedData.length > 0) {
                    canEmit = true;
                    appointmentLogDelayedData = appointmentLogDelayedData[0];
                    isAction = true;
                    actionType = 'delayed';
                    appointmentID = appointmentLogDelayedData.appointment_id;
                    secondsLeft = appointmentLogDelayedData.stamp - currentTime;
                } else if (appointmentLogAcceptedData.length > 0) {
                    canEmit = true;
                    appointmentLogAcceptedData = appointmentLogAcceptedData[0];
                    isAction = true;
                    actionType = 'accepted';
                    appointmentID = appointmentLogAcceptedData.appointment_id;
                    upcomingAppointment = await AppointmentModel.getByID(appointmentID);
                    // upcomingAppointment = upcomingAppointment[0];
                    secondsLeft = upcomingAppointment.start_date_stamp - currentTime;
                } else if (appointmentLogCancelledData.length > 0) {
                    canEmit = true;
                    appointmentLogCancelledData = appointmentLogCancelledData[0];
                    isAction = true;
                    actionType = 'cancelled';
                    appointmentID = appointmentLogCancelledData.appointment_id;
                    upcomingAppointment = await AppointmentModel.getByID(appointmentID);
                    // upcomingAppointment = upcomingAppointment[0];
                    secondsLeft = upcomingAppointment.start_date_stamp - currentTime;
                }
            }
            resolve({
                canEmit: canEmit,
                data: {
                    isAction: isAction,
                    actionType: actionType,
                    secondsLeft: secondsLeft,
                    appointmentID: appointmentID
                }
            });
            // if (canEmit) {
            //     Emitters(socket).appointmentFound({
            //         isAction: isAction,
            //         actionType: actionType,
            //         secondsLeft: secondsLeft,
            //         appointmentID: appointmentID
            //     });
            // }
        } catch (e) {
            console.log(e);
        }
    });
};

appointmentCtrl.checkUpcoming = async (socket, userID) => {
    const appointment = await checkAndGetUpcoming(userID);
    if(appointment.canEmit){
        Emitters(socket).appointmentFound(appointment.data);
    }
    return false;
};

appointmentCtrl.checkIsDoctorAvailableForKnocking = async (socket, appointmentID) => {
    const appointmentKnockingData = await AppointmentKnockingDataModel.isDoctorAvailable(appointmentID);
    Emitters(socket).appointmentIsDoctorAvailableForKnocking(appointmentKnockingData);
    // console.log(appointmentID);
    return false;
};

appointmentCtrl.checkAndGetUpcoming = async (userID) => {
    return new Promise(async (resolve, reject) => {
        const appointment = await checkAndGetUpcoming(userID);
        resolve(appointment);
    });
};

appointmentCtrl.checkDoctorIsIn = async (socket, userID, appointmentID) => {
    const isReady = await AppointmentModel.isDoctorReady(appointmentID, userID);
    if (isReady) {
        Emitters(socket).appointmentDoctorIsReady();
    }
    return false;
};

appointmentCtrl.checkVideoEnd = async (socket, userID, appointmentID) => {
    try {
        const isReady = await AppointmentModel.isDoctorEndConsultation(appointmentID, userID);
        if (!isReady) {
            appointmentCtrl.checkVideoEnd(socket, userID, appointmentID);
        } else {
            Emitters(socket).appointmentDoctorEndConsultation();
        }
    } catch (e) {

    }
};

module.exports = () => {
    return appointmentCtrl;
};