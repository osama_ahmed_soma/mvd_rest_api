const schedule = require('node-schedule');
const moment = require('moment-timezone');

// Models
const UserRegisteredDevicesModel = require('../models/user_registeredDevice');
const MessageModel = require('../models/message');
const NotificationModel = require('../models/notification');
const PushNotificationTempModel = require('../models/pushNotification_temp');
const AppointmentModel = require('../models/appointment');
const PatientModel = require('../models/patient');
const AppointmentLogModel = require('../models/appointment_log');

// Controller
const PushNotificationController = require('../controllers/pushNotification');
const AppointmentController = require('../controllers/appointment');

class CronJobController {

    constructor() {
        this.startForever();
        schedule.scheduleJob('*/1 * * * *', () => {
            this.startForever();
        });
    }

    async startForever() {
        // get all registered devices
        try {
            const registeredDevicesData = await UserRegisteredDevicesModel.getAll();
            if (registeredDevicesData.length > 0) {
                // get all unique userID's
                const uniqueUserIDs = await this.getUniqueUserIDs(registeredDevicesData);
                if (uniqueUserIDs.length > 0) {
                    for (let i = 0; i < uniqueUserIDs.length; i++) {
                        // check for upcoming appointment in 5 minutes
                        // check for appointment status
                        const upcomingAppointment = await AppointmentController().checkAndGetUpcoming(uniqueUserIDs[i]);
                        const patientData = await PatientModel.getByUserID(uniqueUserIDs[i], 'timezone');
                        if (upcomingAppointment.canEmit) {
                            // get appointment data
                            const appointmentData = await AppointmentModel.getByID(upcomingAppointment.data.appointmentID);
                            const startTimeAndDateAppointmentUpcoming = moment.unix(appointmentData.start_date_stamp).tz(patientData.timezone).format('MM/DD/YYYY hh:mm A');
                            if (upcomingAppointment.data.actionType === '' || upcomingAppointment.data.actionType === 'accepted') {
                                // check notification table for appointmentID if id exists then don't do anything and if not then add
                                const notificationDataUpcoming = await NotificationModel.getByAppointmentIDAndType(upcomingAppointment.data.appointmentID, 'appointment:upcoming');
                                if (!notificationDataUpcoming) {
                                    // add upcoming
                                    await NotificationModel.add({
                                        from_userid: appointmentData.doctors_id,
                                        to_userid: uniqueUserIDs[i],
                                        from_usertype: 'doctor',
                                        to_usertype: 'patient',
                                        subject: 'Your appointment is now ready!',
                                        message: 'Your appointment with ' + appointmentData.firstname + ' ' + appointmentData.lastname + ' on ' + startTimeAndDateAppointmentUpcoming + ' is ready to start.',
                                        type: 'appointment:upcoming',
                                        appointmentID: upcomingAppointment.data.appointmentID,
                                        added: moment().tz(patientData.timezone).unix()
                                    });
                                }
                            }
                            if (upcomingAppointment.data.actionType === 'cancelled') {
                                const notificationDataCancelled = await NotificationModel.getByAppointmentIDAndType(upcomingAppointment.data.appointmentID, 'appointment:cancelled');
                                if (!notificationDataCancelled) {
                                    // add cancelled
                                    let appointmentLogData = await AppointmentLogModel.getByAppointmentID(upcomingAppointment.data.appointmentID);
                                    appointmentLogData = appointmentLogData[0];
                                    await NotificationModel.add({
                                        from_userid: appointmentData.doctors_id,
                                        to_userid: uniqueUserIDs[i],
                                        from_usertype: 'doctor',
                                        to_usertype: 'patient',
                                        subject: 'Your appointment has been cancelled!',
                                        message: 'Your appointment with ' + appointmentData.firstname + ' ' + appointmentData.lastname + ' on ' + startTimeAndDateAppointmentUpcoming + ' was cancelled.' + ((appointmentLogData.reason !== '') ? ' Reason: ' + appointmentLogData.reason : ''),
                                        type: 'appointment:cancelled',
                                        appointmentID: upcomingAppointment.data.appointmentID,
                                        added: moment().tz(patientData.timezone).unix()
                                    });
                                }
                            }
                            if (upcomingAppointment.data.actionType === 'delayed') {
                                // add delayed
                                console.log('delayed');
                            }
                        }

                        // check for appointment notes added
                        // get all past past appointment
                        let appointmentPastData = await AppointmentModel.getPast(uniqueUserIDs[i]);
                        if (appointmentPastData.length > 0) {
                            appointmentPastData = appointmentPastData[0];
                            if (appointmentPastData.subjective !== null || appointmentPastData.objective !== null || appointmentPastData.assessment !== null || appointmentPastData.plan !== null) {
                                const notificationDataNotes = await NotificationModel.getByAppointmentIDAndType(appointmentPastData.id, 'appointment:notes');
                                if(!notificationDataNotes) {
                                    await NotificationModel.add({
                                        from_userid: appointmentPastData.doctors_id,
                                        to_userid: uniqueUserIDs[i],
                                        from_usertype: 'doctor',
                                        to_usertype: 'patient',
                                        subject: 'Your Appointment Report is now available',
                                        message: 'Your doctor ' + appointmentPastData.firstname + ' ' + appointmentPastData.lastname + ' has added the report for your last consultation.',
                                        type: 'appointment:notes',
                                        appointmentID: appointmentPastData.id,
                                        added: moment().tz(patientData.timezone).unix()
                                    });
                                }
                            }
                        }


                        // now we have unique user ID's.. search for unread notifications and unread messages
                        // checking unread messages
                        const UnreadMessages = await MessageModel.getUnread(uniqueUserIDs[i]);
                        if (UnreadMessages.length > 0) {
                            const freshMessagesDataForPushNotification = await this.getCanPushData(UnreadMessages);
                            await this.proceedPushNotification(freshMessagesDataForPushNotification, uniqueUserIDs[i]);
                        }
                        // checking local notifications
                        const UnreadNotifications = await NotificationModel.getAll(uniqueUserIDs[i]);
                        // console.log(UnreadNotifications);
                        if (UnreadNotifications.length > 0) {
                            const freshLocalNotificationsDataForPushNotification = await this.getCanPushData(UnreadNotifications, 'notification');
                            await this.proceedPushNotification(freshLocalNotificationsDataForPushNotification, uniqueUserIDs[i], 'notification');
                        }
                    }
                }
            }
        } catch (e) {
            // log error
            console.log(e);
        }
    }

    proceedPushNotification(dataArray = [], uniqueUserID, type = 'message', idField = 'id') {
        return new Promise(async (resolve) => {
            if (dataArray.length > 0) {
                for (let x = 0; x < dataArray.length; x++) {
                    await new PushNotificationController(uniqueUserID, type, dataArray[x]);
                    await PushNotificationTempModel.insert(dataArray[x][idField], type);
                    if (x === (dataArray.length - 1)) {
                        resolve();
                    }
                }
            } else {
                resolve();
            }
        });
    }

    getUniqueUserIDs(data) {
        return new Promise(resolve => {
            const uniqueUserIDs = [];
            if (data.length > 0) {
                for (let i = 0; i < data.length; i++) {
                    if (data[i].userID !== '') {
                        if (uniqueUserIDs.indexOf(data[i].userID) === -1) {
                            uniqueUserIDs.push(data[i].userID);
                        }
                    }
                    if (i === (data.length - 1)) {
                        resolve(uniqueUserIDs);
                    }
                }
            } else {
                resolve(uniqueUserIDs);
            }
        });
    }

    getCanPushData(dataArray = [], type = 'message', idField = 'id') {
        return new Promise(async (resolve) => {
            const uniqueArray = [];
            if (dataArray.length > 0) {
                for (let i = 0; i < dataArray.length; i++) {
                    const PushNotificationTempData = await PushNotificationTempModel.isExists(dataArray[i][idField], type);
                    if (!PushNotificationTempData) {
                        uniqueArray.push(dataArray[i]);
                    }
                    if (i === (dataArray.length - 1)) {
                        resolve(uniqueArray);
                    }
                }
            } else {
                resolve(uniqueArray);
            }
        });
    }
}

module.exports = CronJobController;