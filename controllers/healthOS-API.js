const request = require('request')

const obj = {}

const getAuthToken = () => {
  return new Promise((resolve, reject) => {
    const options = {
      method: 'POST',
      url: 'http://www.healthos.co/api/v1/oauth/token.json',
      headers: {
        'cache-control': 'no-cache',
        'content-type': 'application/json'
      },
      body: {
        grant_type: 'client_credentials',
        client_id: 'bd0be6a7bb4011ef3dc70ebc392876777abaf2cd4c3a84958044b41842bb1770',
        client_secret: '3b9803da6ee9138948b73f9806fe8a01f4514358eded7fd32a349118a96dadd5',
        scope: 'public read write'
      },
      json: true
    }

    request(options, function (error, response, body) {
      if (error) {
        return reject(error)
      }
      resolve(body.access_token)
    })

  })
}

const getMedicines = (token, query) => {
  return new Promise((resolve, reject) => {
    const options = {
      method: 'GET',
      url: `http://www.healthos.co/api/v1/autocomplete/medicines/brands/${query}`,
      headers: {
        'cache-control': 'no-cache',
        authorization: `Bearer ${token}`
      }
    }

    request(options, function (error, response, body) {
      if (error) {
        return reject(error)
      }
      resolve(JSON.parse(body))
    })
  })
}

obj.getData = query => new Promise(async (resolve, reject) => {
  try {
    const token = await getAuthToken()
    const medicines = await getMedicines(token, query)
    resolve(medicines)
  } catch (e) {
    reject(e)
  }
})

module.exports = obj