const PushNotifications = require('node-pushnotifications');
const moment = require('moment-timezone');

// Models
const UserModel = require('../models/user');
const UserRegisteredDevicesModel = require('../models/user_registeredDevice');
const DoctorModel = require('../models/doctor');
const PatientModel = require('../models/patient');
const AppointmentModel = require('../models/appointment');
const AppointmentLogModel = require('../models/appointment_log');
const ReasonModel = require('../models/reason');

class PushNotification {

    constructor(userID, type, data) {
        return new Promise(async (resolve, reject) => {
            try {
                this.settings = {
                    gcm: {
                        id: 'AAAAlb6_gog:APA91bFi4D4UTQkf5lDcQTPW1MuYbX8hpVvXJpx7Ek8AmkRDo02bS2xYeHeugDwXMkXqy4dbj-whSGcs0WNerrMf_OEjv3a7S-t4joDtYxIlQ8GQgyPcRsQAkNfB-yG779iFCsNq31Ow'
                    },
                    apn: {
                        // token: {
                        //     key: '../certs/AuthKey_5544L3KL98.p8', // optionally: fs.readFileSync('./certs/key.p8')
                        //     keyId: '5544L3KL98',
                        //     teamId: '34TT5825BB'
                        // },
                        // cert: 'certs/PushAppstoreCert.pem'
                        production: true
                    }
                };
                this.userID = userID;
                this.type = type;
                this.data = data;
                this.push = new PushNotifications(this.settings);
                await this.init();
                resolve();
            } catch (e) {
                reject(e);
            }
        });
    }

    init() {
        return new Promise(async (resolve, reject) => {
            try {
                const isUserExists = await UserModel.isExists(this.userID);
                if (isUserExists) {
                    const registeredDevicesData = await UserRegisteredDevicesModel.getByUserID(this.userID);
                    // loop all devices
                    if (registeredDevicesData.length > 0) {
                        for (let i = 0; i < registeredDevicesData.length; i++) {
                            // registrationIds.push(registeredDevicesData[i].deviceID);
                            const registrationIds = registeredDevicesData[i].deviceID;
                            const messageNotify = registeredDevicesData[i].messageNotify;
                            const appointmentNotify = registeredDevicesData[i].appointmentNotify;

                            let dataForPushNotification;
                            switch (this.type) {
                                case 'message':
                                    if (messageNotify === 'no') {
                                        resolve();
                                        continue;
                                    }
                                    const doctorData = await DoctorModel.getByUserID(this.data.from_userid);
                                    const patientDataMessage = await PatientModel.getByUserID(this.userID);
                                    const formatDateTime = moment.unix(this.data.added).tz(patientDataMessage.timezone).format('MM/DD/YYYY hh:mm A');
                                    dataForPushNotification = {
                                        title: doctorData.firstname + ' ' + doctorData.lastname + ' sent a message on ' + formatDateTime,
                                        body: this.stripHtml(this.data.message),
                                        sound: 'default',
                                        contentAvailable: true,
                                        topic: 'com.ionicframework.mvdmobileapp642926',
                                        custom: {
                                            type: this.type,
                                            messageData: this.data
                                        }
                                    };
                                    break;
                                case 'notification':
                                    if (appointmentNotify === 'no') {
                                        resolve();
                                        continue;
                                    }
                                    let localType = this.type;
                                    let localTitle = 'New Session Requested';
                                    let localCustomObj = {
                                        type: localType
                                    };
                                    if (this.data.appointmentID !== null) {
                                        // get appointment data
                                        localType = this.data.type;
                                        localTitle = this.data.subject;
                                    }
                                    if (localType === 'appointment:upcoming') {
                                        // get appointment data
                                        const appointmentDataUpcoming = await AppointmentModel.getByID(this.data.appointmentID);
                                        localCustomObj = {
                                            type: localType,
                                            appointment: {
                                                upcoming: appointmentDataUpcoming
                                            }
                                        };
                                    }
                                    if (localType === 'appointment:cancelled') {
                                        const appointmentDataCancelled = await AppointmentModel.getByID(this.data.appointmentID);
                                        localCustomObj = {
                                            type: localType,
                                            appointment: {
                                                cancelled: appointmentDataCancelled
                                            }
                                        };
                                    }
                                    if (localType === 'appointment:notes') {
                                        const appointmentDataNotes = await AppointmentModel.getByID(this.data.appointmentID);
                                        appointmentDataNotes.reasonObject = await ReasonModel.getByID(appointmentDataNotes.reason_id);
                                        localCustomObj = {
                                            type: localType,
                                            appointment: {
                                                notes: appointmentDataNotes
                                            }
                                        };
                                    }
                                    dataForPushNotification = {
                                        title: localTitle,
                                        body: this.stripHtml(this.data.message),
                                        sound: 'default',
                                        contentAvailable: true,
                                        topic: 'com.ionicframework.mvdmobileapp642926',
                                        custom: localCustomObj
                                    };
                                    break;
                            }
                            // console.log(JSON.stringify(dataForPushNotification));
                            this.push.send(registrationIds, dataForPushNotification, async (err, result) => {
                                if (err) {
                                    console.log(err);
                                    // reject(err);
                                } else {
                                    if (result[0].failure === 1) {
                                        // remove regID from database
                                        await UserRegisteredDevicesModel.removeByDeviceID(registrationIds);
                                    }

                                }
                                if (i === (registeredDevicesData.length - 1)) {
                                    resolve();
                                }
                            });
                            // resolve();
                        }
                    }
                }
            } catch (e) {
                console.log(e);
                reject(e);
            }
        });
    }

    stripHtml(string) {
        return string.replace(/(<([^>]+)>)/ig, "");
    }

}

module.exports = PushNotification;