const mysql = require('mysql2');
const config = require('./config');

const dbPool = mysql.createPool(config.database);

module.exports = dbPool;