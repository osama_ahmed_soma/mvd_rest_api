const express = require('express');
const router = express.Router();

// Models
const NotificationModel = require('../models/notification');

router.get('/readAll', async (req, res) => {
    try {
        const userID = req.userData.userid;
        const type = 'patient';
        await NotificationModel.updateByUserIDAndType(userID, type, {
            view: 1
        });
        res.json({
            message: '',
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});

router.put('/read', async (req, res) => {
    try {
        const notificationID = req.body.notificationID;
        await NotificationModel.updateByID(notificationID, {
            view: 1
        });
        res.json({
            message: '',
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});

module.exports = router;