const express = require('express')
const router = express.Router()

// Models
const SurgeriesDataModel = require('../models/surgeries_data_tbl')

router.get('/', async (req, res) => {
  try {
    const surgeriesData = await SurgeriesDataModel.getAll()
    res.json({
      message: surgeriesData,
      bool: true
    })
  } catch (e) {
    res.json(e)
  }
})

module.exports = router