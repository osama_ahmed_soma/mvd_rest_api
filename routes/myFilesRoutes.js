const path = require('path');
const fs = require('fs');
const express = require('express');
const router = express.Router();
const moment = require('moment-timezone');
const filesize = require('filesize');
const multer = require('multer');
const checkDocDirAndMakeRecursive = (initPath, array, index) => {
    return new Promise(async resolve => {
        if (index == (array.length + 1)) {
            return resolve();
        }
        if (fs.existsSync(initPath)) {
            await checkDocDirAndMakeRecursive(initPath + path.sep + array[index], array, index + 1);
            return resolve();
        } else {
            fs.mkdirSync(initPath);
            await checkDocDirAndMakeRecursive(initPath, array, index);
            return resolve();
        }
    });
};
let upload = multer({
    storage: multer.diskStorage({
        destination: async (req, file, callback) => {
            await checkDocDirAndMakeRecursive(config.basePath + path.sep + 'uploads' + path.sep + 'documents', [
                'patient',
                req.userData.userid
            ], 0);
            callback(null, config.basePath + path.sep + 'uploads' + path.sep + 'documents' + path.sep + 'patient' + path.sep + req.userData.userid + path.sep);
        },
        filename: (req, file, callback) => {
            //originalname is the uploaded file's name with extn
            callback(null, file.originalname);
        }
    })
});

// Config
const config = require('../config/config');

// Models
const FilesUploadModel = require('../models/files_upload');

// Routes
const SharedFilesRoutes = require('./sharedFilesRoutes');

router.get('/', async (req, res) => {
    try {
        const patientID = req.userData.userid;
        let files = await FilesUploadModel.getAll(patientID);
        const filterFiles = files => {
            return new Promise(resolve => {
                if (files.length > 0) {
                    for (let i = 0; i < files.length; i++) {
                        files[i].date_time = moment.tz(files[i].date_time, process.env.TZ).format('L');
                        files[i].filesize = filesize(files[i].filesize);
                        if (i == (files.length - 1)) {
                            resolve(files);
                        }
                    }
                } else {
                    resolve(files);
                }
            });
        };
        res.send({
            message: {
                files: await filterFiles(files)
            },
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});
router.post('/upload', upload.single('file'), async (req, res) => {
    try {
        await FilesUploadModel.insert({
            patientID: req.userData.userid,
            file: {
                name: req.file.filename,
                size: req.file.size,
                type: req.file.mimetype
            }
        });
        res.json({
            message: '',
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});
router.post('/:fileID', async (req, res) => {
    try {
        const fileID = req.params.fileID;
        const title = req.body.title;
        await FilesUploadModel.update(fileID, {
            title: title
        });
        res.json({
            message: 'Updated successfully',
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});
router.delete('/:fileID', async (req, res) => {
    try {
        const fileID = req.params.fileID;
        const patientID = req.userData.userid;
        await FilesUploadModel.remove(fileID, patientID);
        res.json({
            message: 'Document deleted successfully.',
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});
router.get('/download/:fileID', async (req, res) => {
    try {
        const fileID = req.params.fileID;
        const patientID = req.userData.userid;
        let file = await FilesUploadModel.getByID(fileID);
        if (file.length <= 0) {
            return res.json({
                message: 'File not exists.',
                bool: true
            });
        }
        file = file[0];
        res.download(config.basePath + path.sep + 'uploads' + path.sep + 'documents' + path.sep + 'patient' + path.sep + patientID + path.sep + file.filename, file.filename);
    } catch (e) {
        res.json(e);
    }
});
router.use('/sharedFiles/:fileID', (req, res, next) => {
    req.fileID = req.params.fileID;
    next();
});
router.use('/sharedFiles/:fileID', SharedFilesRoutes);
module.exports = router;