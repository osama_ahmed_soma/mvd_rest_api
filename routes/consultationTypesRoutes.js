const express = require('express');
const router = express.Router();

// Models
const DoctorConsultationTypesModel = require('../models/doctor_consultation_types');

router.get('/:doctorID', async (req, res) => {
    try {
        const consultationTypes = await DoctorConsultationTypesModel.getByDoctorID(req.params.doctorID);
        res.json({
            message: consultationTypes,
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});

module.exports = router;