const express = require('express');
const router = express.Router();
const moment = require('moment-timezone');

// Config
const config = require('../config/config');

// Models
const Doctor = require('../models/doctor');
const Appointment = require('../models/appointment');
const Favorite = require('../models/favorite');

// Helpers
const phpNative = require('../helpers/php-js');

router.get('/:doctor_user_id', async (req, res) => {
    try {
        const doctor_user_id = req.params.doctor_user_id;
        const doctor = await Doctor.getByUserID(doctor_user_id);
        doctor.specials = await Doctor.getSpecializationByUserID(doctor_user_id);
        doctor.educations = await Doctor.getEducation(doctor_user_id);
        doctor.awards = await Doctor.getAward(doctor_user_id);
        doctor.licenced_states = await Doctor.getOtherLicense(doctor_user_id);
        doctor.languages = await Doctor.getLanguage(doctor_user_id);
        doctor.isFav = await Favorite.getByDoctorID(req.userData.userid, doctor_user_id, true);
        delete doctor.user_password;
        if(doctor.image_url){
            doctor.image_url = config.originalServer + '' + doctor.image_url;
        }
        res.json(doctor);
    } catch (e) {
        res.json(e);
    }
});

router.get('/available-time/:doctor_id/:year/:month/:day', async (req, res) => {
    try {

        const available_time = await Doctor.getAvailableTime(req.params.doctor_id, {
            year: req.params.year,
            month: req.params.month,
            day: req.params.day
        });
        const result = [];
        for (let i = 0; i < available_time.length; i++) {
            let temp_start_date = available_time[i].start_date_stamp;
            let temp_end_date = moment.tz((temp_start_date * 1000), process.env.TZ).add(30, 'minutes').unix();
            if (phpNative.round((phpNative.abs(available_time[i].end_date_stamp - available_time[i].start_date_stamp) / 60), 2) > 30) {
                while (temp_end_date <= available_time[i].end_date_stamp) {
                    if (await Appointment.checkAppointmentAvailable(available_time[i].userid, temp_start_date, temp_end_date) === 0) {
                        const now = moment().tz(process.env.TZ).unix();
                        if (temp_start_date > now) {
                            const start_time = moment.tz((temp_start_date * 1000), process.env.TZ);
                            const end_time = moment.tz((temp_end_date * 1000), process.env.TZ);
                            const full_date = moment.tz((temp_start_date * 1000), process.env.TZ).format('dddd, MMMM DD YYYY');
                            result.push({
                                availableID: available_time[i].id,
                                start_time_stamp: start_time.unix(),
                                end_time_stamp: end_time.unix(),
                                start_time: start_time.format('hh:mm A'),
                                end_time: end_time.format('hh:mm A'),
                                date: available_time[i].start_date,
                                full_date: full_date,
                                start_date: temp_start_date,
                                end_date: temp_end_date
                            });
                        }
                    }
                    temp_start_date = temp_end_date;
                    temp_end_date = moment.tz((temp_start_date * 1000), process.env.TZ).add(30, 'minutes').unix();
                }
            } else {
                if (await Appointment.checkAppointmentAvailable(available_time[i].userid, available_time[i].start_date_stamp, available_time[i].end_date_stamp) === 0) {
                    const now = moment().tz(process.env.TZ).unix();
                    if (temp_start_date > now) {
                        const start_time = moment.tz((available_time[i].start_date_stamp * 1000), process.env.TZ);
                        const end_time = moment.tz((available_time[i].end_date_stamp * 1000), process.env.TZ);
                        const full_date = moment.tz((temp_start_date * 1000), process.env.TZ).format('dddd, MMMM DD YYYY');
                        result.push({
                            availableID: available_time[i].id,
                            start_time_stamp: start_time.unix(),
                            end_time_stamp: end_time.unix(),
                            start_time: start_time.format('hh:mm A'),
                            end_time: end_time.format('hh:mm A'),
                            date: available_time[i].start_date,
                            full_date: full_date,
                            start_date: temp_start_date,
                            end_date: temp_end_date
                        });
                    }
                }
            }
        }
        const currentDate = moment().tz(process.env.TZ).format('YYYY-MM-DD');
        res.json({
            message: '',
            times: result,
            currentDate: currentDate,
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});

router.post('/search', async (req, res, next) => {
    try {
        const query = req.body.query || '';
        const phone_number = req.body.phone_number || '';
        const state = req.body.state || '';
        const specialty = req.body.specialty || '';
        const online_now = req.body.online_now;
        if (query === '' && state === '' && specialty === '' && phone_number === '' && !online_now) {
            return res.json({
                message: "You must search with Doctor Name, State Specialty or Phone Number.",
                bool: false
            });
        }
        const searchResult = await Doctor.searchDoctor({
            query: query,
            state: state,
            specialty: specialty,
            online_now: online_now
        });
        let flags = [], output = [], l = searchResult.length, i;
        for (i = 0; i < l; i++) {
            if (flags[searchResult[i].id]) continue;
            flags[searchResult[i].id] = true;
            searchResult[i].isFav = await Favorite.getByDoctorID(req.userData.userid, searchResult[i].userid, true);
            if(searchResult[i].image_url){
                searchResult[i].image_url = config.originalServer + '' + searchResult[i].image_url;
            }
            output.push(searchResult[i]);
        }
        res.send({
            message: '',
            search_result: output,
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});

module.exports = router;