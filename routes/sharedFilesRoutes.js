const express = require('express');
const router = express.Router();
const moment = require('moment-timezone');

// Models
const SharedFilesModel = require('../models/shared_files');

router.get('/', async (req, res) => {
    try {
        const fileID = req.fileID;
        const patientID = req.userData.userid;
        const files = await SharedFilesModel.getByFileIDAndPatientID(fileID, patientID);
        const filterFiles = files => {
            return new Promise(resolve => {
                if (files.length > 0) {
                    for (let i = 0; i < files.length; i++) {
                        files[i].date_time = moment.tz(files[i].date_time, process.env.TZ).format('L');
                        if (i == (files.length - 1)) {
                            resolve(files);
                        }
                    }
                } else {
                    resolve(files);
                }
            });
        };
        res.json({
            message: {
                files: await filterFiles(files)
            },
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});
router.post('/', async (req, res) => {
    try {
        const fileID = req.fileID;
        const patientID = req.userData.userid;
        const doctorID = req.body.doctorID;
        const sharedFile = await SharedFilesModel.getByFileIDAndPatientIDAndDoctorID(fileID, patientID, doctorID);
        if (sharedFile.length > 0) {
            return res.json({
                message: 'Already shared.',
                bool: false
            });
        }
        await SharedFilesModel.insert(fileID, doctorID, patientID);
        res.json({
            message: 'Shared successfully',
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});
module.exports = router;