const express = require('express');
const router = express.Router();
const moment = require('moment-timezone');
// Models
const MessageModel = require('../models/message');

router.get('/', async (req, res) => {

});

router.post('/', async (req, res) => {
    try {
        const body = {
            patientID: req.userData.userid,
            recipientID: req.body.recipientID,
            subject: req.body.subject,
            message: req.body.message
        };
        const messages = await MessageModel.insert(body);
        res.json({
            message: 'Message sent successfully.',
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});

router.get('/view/:messageID/:type', async (req, res) => {
    try {
        const patientID = req.userData.userid;
        const messageID = req.params.messageID;

        let message = await MessageModel.getByID(messageID);
        if (message.length <= 0) {
            return res.json({
                message: 'Invalid message selected.',
                bool: false
            });
        }
        message = message[0];

        //check message is viewed or not
        if (message.to_userid == patientID && message.opened == 0) {
            await MessageModel.update(messageID, {
                opened: 1
            });
        }

        message = await MessageModel.fixMessageForDisplay(message, patientID);

        //now check for reply
        let replies = [];
        if (message.parent > 0) {
            replies = await MessageModel.getLatestFiveReplysForMessage(message.parent, patientID, replies);
        }
        message.added = moment.unix(message.added).tz(process.env.TZ).fromNow();
        if(replies.length > 0){
            for(let i = 0; i < replies.length; i++){
                replies[i].added = moment.unix(replies[i].added).tz(process.env.TZ).fromNow();
                if(i == (replies.length - 1)){
                    res.json({
                        message: {
                            messages: {
                                message: message,
                                replies: replies
                            }
                        },
                        bool: true
                    });
                }
            }
        } else {
            res.json({
                message: {
                    messages: {
                        message: message,
                        replies: replies
                    }
                },
                bool: true
            });
        }
    } catch (e) {
        res.json(e);
    }
});

router.get('/inbox', async (req, res) => {
    try {
        const messages = await MessageModel.getInbox(req.userData.userid);
        res.json({
            message: {
                messages: messages
            },
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});

router.get('/sent', async (req, res) => {
    try {
        const messages = await MessageModel.getSent(req.userData.userid);
        res.json({
            message: {
                messages: messages
            },
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});
router.get('/drafts', async (req, res) => {
    try {
        const messages = await MessageModel.getDraft(req.userData.userid);
        res.json({
            message: {
                messages: messages,
            },
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});
router.get('/trash', async (req, res) => {
    try {
        const messages = await MessageModel.getTrash(req.userData.userid);
        res.json({
            message: {
                messages: messages,
            },
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});
router.get('/unread-count', async (req, res) => {
    try {
        const UnreadMessagesCount = await MessageModel.getUnreadCount(req.userData.userid);
        res.json({
            message: UnreadMessagesCount,
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});
router.post('/search', async (req, res) => {
    try {
        const searchText = req.body.search.trim() || '';
        const type = req.body.type.trim() || '';
        const patientID = req.userData.userid;
        let messages = '';
        switch (type) {
            case 'inbox':
                messages = await MessageModel.searchInboxMessages(searchText, patientID);
                break;
            case 'sent':
                messages = await MessageModel.searchSentMessages(searchText, patientID);
                break;
            case 'drafts':
                messages = await MessageModel.searchDraftMessages(searchText, patientID);
                break;
            case 'trash':
                messages = await MessageModel.searchTrashMessages(searchText, patientID);
                break;
            default:
                return res.json({
                    message: 'Wrong type define.',
                    bool: false
                });
                break;
        }
        res.json({
            message: {
                messages: messages,
            },
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});
router.delete('/:messageID', async (req, res) => {
    try {
        const patientID = req.userData.userid;
        const messageID = req.params.messageID;
        await MessageModel.moveToTrash(messageID, patientID);
        res.json({
            message: 'Message deleted successfully.',
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});
router.delete('/permanent/:messageID', async (req, res) => {
    try {
        const patientID = req.userData.userid;
        const messageID = req.params.messageID;
        await MessageModel.permanentDelete(messageID, patientID);
        res.json({
            message: 'Message deleted successfully.',
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});

module.exports = router;