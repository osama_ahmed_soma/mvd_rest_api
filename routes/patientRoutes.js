const express = require('express');
const router = express.Router();
const moment = require('moment-timezone');
const base64Img = require('base64-img');
const path = require('path');
const uuid = require('uuid/v4');

// Config
const config = require('../config/config');

// Models
const PatientModel = require('../models/patient');
const CountryModel = require('../models/country');
const StateModel = require('../models/state');
const TimeZonesModel = require('../models/timezones');
const FavoriteModel = require('../models/favorite');
const DoctorModel = require('../models/doctor');
const DoctorSpecializationModel = require('../models/doctor_specialization');
const DoctorDegreeModel = require('../models/doctor_degree');
const UserModel = require('../models/user');

// Controller
const ProfileCompletionController = require('../controllers/profile-completion');

// Routes
const MyHealthRoutes = require('./myHealthRoutes');
const MyFilesRoutes = require('./myFilesRoutes');

router.get('/', async (req, res) => {
    try {
        const patientID = req.userData.userid;
        let patientData = await PatientModel.getByUserID(patientID);
        if (patientData.image_url) {
            patientData.image_url = config.originalServer + '' + patientData.image_url;
        }
        if (patientData.dob) {
            patientData.dob = moment(patientData.dob).format('YYYY-MM-DD');
        }
        const countries = await CountryModel.getAll();
        const states = await StateModel.getAll();
        const timeZones = await TimeZonesModel.getAll();
        const profileCompletionData = await ProfileCompletionController.getProfileCompletion(patientID);
        res.json({
            message: {
                patientData: patientData,
                countries: countries,
                states: states,
                timeZones: timeZones,
                profileCompletionData: profileCompletionData
            },
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});

router.put('/', async (req, res) => {
    try {
        const patientID = req.userData.userid;
        const data = req.body;
        if (data.firstName.trim() == '') {
            return res.json({
                message: 'First Name is required.',
                bool: false
            });
        }
        if (data.lastName.trim() == '') {
            return res.json({
                message: 'Last Name is required.',
                bool: false
            });
        }
        if (data.city.trim() == '') {
            return res.json({
                message: 'City is required.',
                bool: false
            });
        }
        if (data.country.trim() == '') {
            return res.json({
                message: 'Country is required.',
                bool: false
            });
        }
        if (data.country.trim() == 'United States') {
            if (!data.stateID) {
                return res.json({
                    message: 'State is required.',
                    bool: false
                });
            }
        }
        if (!data.gender) {
            return res.json({
                message: 'Gender is required.',
                bool: false
            });
        }
        if (data.timeZone.trim() == '') {
            return res.json({
                message: 'Time Zone is required.',
                bool: false
            });
        }
        if (data.cellPhone.trim() == '') {
            return res.json({
                message: 'Cell Phone is required.',
                bool: false
            });
        }
        if (data.weight <= 0) {
            return res.json({
                message: 'Weight is required.',
                bool: false
            });
        }
        if (data.height.feet <= 0) {
            return res.json({
                message: 'Height Ft. is required.',
                bool: false
            });
        }
        if (data.height.inches < 0) {
            return res.json({
                message: 'Height In. is required.',
                bool: false
            });
        }
        if(data.optionalState == null){
            data.optionalState = '';
        }
        await PatientModel.updateByUserID(patientID, {
            firstname: data.firstName.trim(),
            lastname: data.lastName.trim(),
            address: data.address.trim(),
            city: data.city.trim(),
            optional_state: data.optionalState.trim(),
            state_id: data.stateID,
            zip: data.zip.trim(),
            country: data.country.trim(),
            dob: moment(data.dob.trim()).format('MM/DD/YYYY'),
            gender: data.gender,
            timezone: data.timeZone.trim(),
            phone_mobile: data.cellPhone.trim(),
            weight: data.weight,
            height_ft: data.height.feet,
            height_inch: data.height.inches,
        });
        res.json({
            message: 'Profile updated.',
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});

router.put('/image', async (req, res) => {
    try {
        const patientID = req.userData.userid;
        const image_url = req.body.image_url;
        if (image_url == '') {
            return res.json({
                message: 'Image Data is required.',
                bool: false
            });
        }
        const fileName = uuid();
        base64Img.img(image_url, config.basePath + path.sep + 'uploads' + path.sep + 'patients' + path.sep, fileName, async (err, filePath) => {
            if (err) {
                return res.json({
                    message: 'There is an error in image.',
                    bool: false
                });
            }
            try {
                await PatientModel.updateByUserID(patientID, {
                    image_url: 'uploads/patients/' + fileName + '.jpg'
                });
                res.json({
                    message: 'Image Update Successfully.',
                    bool: true
                });
            } catch (e) {
                res.json(e);
            }
        });
    } catch (e) {
        res.json(e);
    }
});

router.get('/my-doctors', async (req, res) => {
    try {
        const favoriteData = await FavoriteModel.getMyDoctors(req.userData.userid);
        const mydoctors = [];
        if (favoriteData.length > 0) {
            for (let i = 0; i < favoriteData.length; i++) {
                const doctorData = await DoctorModel.getByUserID(favoriteData[i].d_id, '`userid`, `firstname`, `lastname`, `country`, `image_url`, `location`, `stripe_one`, `stripe_two`, `online_status`', true);
                if (doctorData.length > 0) {
                    doctorData[0].specialty = await DoctorSpecializationModel.getByDoctorID(favoriteData[i].d_id);
                    doctorData[0].degree = await DoctorDegreeModel.getByUserID(favoriteData[i].d_id);
                    if (doctorData[0].image_url) {
                        doctorData[0].image_url = config.originalServer + '' + doctorData[0].image_url;
                    }
                    mydoctors.push(doctorData[0]);
                    if (i == (favoriteData.length - 1)) {
                        res.send({
                            message: '',
                            myDoctors: mydoctors,
                            bool: true
                        });
                    }
                }
            }
        } else {
            res.send({
                message: '',
                myDoctors: favoriteData,
                bool: true
            });
        }
    } catch (e) {
        res.json(e);
    }
});

router.post('/addDoctor', async (req, res) => {
    try {
        await FavoriteModel.add(req.userData.userid, req.body.doctorID);
        res.send({
            message: 'Doctor added to My Physicians',
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});

router.delete('/removeDoctor', async (req, res) => {
    try {
        await FavoriteModel.remove(req.userData.userid, req.body.doctorID);
        res.send({
            message: 'Doctor Removed from My Physicians',
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});

router.get('/getPrimaryPhysician', async (req, res) => {
    try {
        const patientID = req.userData.userid;
        const states = await StateModel.getAll();
        let primaryPhysician = await DoctorModel.getByPatientID(patientID);
        if (primaryPhysician.length <= 0) {
            primaryPhysician = {};
        } else {
            primaryPhysician = primaryPhysician[0];
        }
        res.json({
            message: {
                states: states,
                primaryPhysician: primaryPhysician
            },
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});

router.put('/updatePrimaryPhysician', async (req, res) => {
    try {
        const patientID = req.userData.userid;
        const body = req.body;
        let primaryPhysician = await DoctorModel.getByPatientID(patientID);
        if (primaryPhysician.length <= 0) {
            // create

            // await DoctorModel.create(body);
        } else {
            primaryPhysician = primaryPhysician[0];
            // update
            await UserModel.update(primaryPhysician.userid, {
                user_email: body.email
            });
            await DoctorModel.update(primaryPhysician.userid, body);
        }
        res.json({
            message: 'Primary Physician has been updated.',
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});


router.use('/myFiles', MyFilesRoutes);

router.use('/myHealth', MyHealthRoutes);

module.exports = router;