const express = require('express');
const router = express.Router();

// Models
const WaiveFee = require('../models/waive_fee');

// get by doctor
router.get('/:doctorID', async (req, res) => {
    try {
        const patientID = req.userData.userid;
        const doctorID = req.params.doctorID;
        const waiveFee = await WaiveFee.getByDoctorAndPatientID(doctorID, patientID);
        res.send({
            message: '',
            reasons: reasons,
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});

router.get('/is-patient-waived/:doctorID', async (req, res) => {
    try {
        const patientID = req.userData.userid;
        const doctorID = req.params.doctorID;
        const waiveFee = await WaiveFee.isPatientWaived(doctorID, patientID);
        res.send({
            message: (waiveFee == 1) ? 'Waive Fee Enabled' : 'Waive Fee Disabled',
            bool: (waiveFee == 1) ? true : false
        });
    } catch (e) {
        res.json(e);
    }
});

module.exports = router;