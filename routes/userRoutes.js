const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();

// Config
const config = require('../config/config');

// Middleware
const authorized = require('../middleware/authorized');

// Models
const User = require('../models/user');
const PatientModel = require('../models/patient');
const UserRegisteredDeviceModel = require('../models/user_registeredDevice');

router.post('/login', async (req, res, next) => {
    const email = req.body.email;
    const password = req.body.password;
    const deviceID = req.body.deviceID;
    const deviceType = req.body.deviceType;
    if (!email || !password) {
        return res.json({
            message: 'Email and Password is required.',
            bool: false
        });
    }
    try {
        const userData = await User.login({
            email: email,
            password: password
        });
        if (!userData.bool) {
            return res.json(userData);
        }
        const user = userData.userData;
        const currentDateObj = new Date();
        const expiresIn = 60 * 60 * 24;
        // const expiresIn = 30;
        // get user state_id
        const patientData = await PatientModel.getByUserID(user.userid);
        user.state_id = patientData.state_id;
        user.expiresIn = currentDateObj.getTime() + (1000 * expiresIn);
        const token = jwt.sign(user, config.secret, {
            expiresIn: expiresIn
        });
        if (deviceID && deviceType) {
            const isUserRegisteredDevice = await UserRegisteredDeviceModel.isDeviceExists(deviceID);
            if (isUserRegisteredDevice) {
                // update device id with userID
                await UserRegisteredDeviceModel.updateByDeviceID({
                    'userID': user.userid
                }, deviceID);
            } else {
                // insert new
                await UserRegisteredDeviceModel.insert(deviceID, user.userid, deviceType);
            }
        }
        res.json({
            message: userData.message,
            token: token,
            userID: user.userid,
            bool: true
        });
    } catch (e) {
        return res.json(e);
    }
});

router.post('/register', async (req, res, next) => {
    const firstName = req.body.first_name;
    const lastName = req.body.last_name;
    const email = req.body.email;
    if (!firstName || !lastName || !email) {
        return res.json({
            message: 'First Name, Last Name and Email is required.',
            bool: false
        });
    }
    try {
        const data = await User.signup({
            first_name: firstName,
            last_name: lastName,
            email: email
        });
        res.json(data);
    } catch (e) {
        return res.json(e);
    }
});

router.post('/forgot-password', async (req, res) => {
    const email = req.body.email;
    if (!email) {
        return res.json({
            message: 'Email is required.',
            bool: false
        });
    }
    try {
        const data = await User.generateToken(email);
        res.json(data);
    } catch (e) {
        return res.json(e);
    }
});

router.post('/change-password', async (req, res) => {
    const token = req.body.token;
    const email = req.body.email;
    const password = req.body.password;
    try {
        const data = await User.changePassword(token, email, password);
        res.json(data);
    } catch (e) {
        return res.json(e);
    }
});

router.put('/changePassword', authorized, async (req, res) => {
    try {
        const userID = req.userData.userid;
        const passwordData = req.body;
        if (passwordData.currentPassword.trim() == '') {
            return res.json({
                message: 'Current Password is required.',
                bool: false
            });
        }
        if (passwordData.newPassword.trim() == '') {
            return res.json({
                message: 'New Password is required.',
                bool: false
            });
        }
        if (passwordData.newPasswordAgain.trim() == '') {
            return res.json({
                message: 'New Password Again is required.',
                bool: false
            });
        }
        if (passwordData.newPassword.trim() != passwordData.newPasswordAgain.trim()) {
            return res.json({
                message: 'Passwords are not same.',
                bool: false
            });
        }
        const data = await User.newPassword(userID, passwordData.currentPassword.trim(), passwordData.newPassword.trim());
        res.json(data);
    } catch (e) {
        return res.json(e);
    }
});

router.post('/logout', authorized, async (req, res) => {
    const userID = req.userData.userid;
    const deviceID = req.body.deviceID;
    try {
        await UserRegisteredDeviceModel.removeByDeviceIDAndUserID(deviceID, userID);
        res.json({
            message: '',
            bool: true
        });
    } catch (e) {
        return res.json(e);
    }
});

module.exports = router;