const express = require('express')
const router = express.Router()
const moment = require('moment-timezone')

// Models
const HealthModel = require('../models/health')
const AllergiesModel = require('../models/allergies')

// Routes
const AllergiesDataRoutes = require('./allergiesDataRoutes')

router.use('/allergies_data', AllergiesDataRoutes)

router.get('/', async (req, res) => {
  try {
    const allergiesData = await HealthModel.getData({
      patientID: req.userData.userid,
      type: 'allergies'
    })
    res.json({
      message: allergiesData,
      bool: true
    })
  } catch (e) {
    res.json(e)
  }
})

router.get('/pre_defined', async (req, res) => {
  try {
    // get all pre defined allergies
    const allergies = await AllergiesModel.getAll()
    res.json({
      message: allergies,
      bool: true
    })
  } catch (e) {
    res.json(e)
  }
})

router.post('/', async (req, res) => {
  try {
    let condition_term = req.body.condition_term.trim() || ''
    const multiple_condition_terms = req.body.multiple_condition_terms || []
    const source = req.body.source.trim() || ''
    const severity = req.body.severity.trim() || ''
    const reaction = req.body.reaction.trim() || ''
    let isOther = false
    if (multiple_condition_terms.length <= 0 || source == '' || severity == '' || reaction == '') {
      return res.json({
        message: 'Please fill all fields.',
        bool: false
      })
    } else {
      if (multiple_condition_terms.indexOf('other') !== -1) {
        isOther = true
        if (condition_term == '') {
          return res.json({
            message: 'Please fill all fields.',
            bool: false
          })
        }
      }
    }
    if (isOther) {
      if (multiple_condition_terms.length > 1) {
        // remove other in array and insert data all array data
        multiple_condition_terms.splice(multiple_condition_terms.indexOf('other'), 1)
        for (let i = 0; i < multiple_condition_terms.length; i++) {
          await HealthModel.insert({
            '`type`': 'allergies',
            '`condition_term`': multiple_condition_terms[i],
            '`date`': moment().tz(process.env.TZ).format('MM/DD/YYYY'),
            '`source`': source,
            '`severity`': severity,
            '`reaction`': reaction,
            '`patients_id`': req.userData.userid
          })
          if (i === (multiple_condition_terms.length - 1)) {
            await HealthModel.insert({
              '`type`': 'allergies',
              '`condition_term`': condition_term,
              '`date`': moment().tz(process.env.TZ).format('MM/DD/YYYY'),
              '`source`': source,
              '`severity`': severity,
              '`reaction`': reaction,
              '`patients_id`': req.userData.userid
            })

            // insert in pre defined allergies if not exists
            await AllergiesModel.insertIfNotExists(condition_term)
            return res.json({
              message: 'Inserted Successfully',
              bool: true
            })
          }
        }
      } else {
        // add only one time
        await HealthModel.insert({
          '`type`': 'allergies',
          '`condition_term`': condition_term,
          '`date`': moment().tz(process.env.TZ).format('MM/DD/YYYY'),
          '`source`': source,
          '`severity`': severity,
          '`reaction`': reaction,
          '`patients_id`': req.userData.userid
        })

        // insert in pre defined allergies if not exists
        await AllergiesModel.insertIfNotExists(condition_term)
        return res.json({
          message: 'Inserted Successfully',
          bool: true
        })
      }
    } else {
      // loop array
      for (let i = 0; i < multiple_condition_terms.length; i++) {
        await HealthModel.insert({
          '`type`': 'allergies',
          '`condition_term`': multiple_condition_terms[i],
          '`date`': moment().tz(process.env.TZ).format('MM/DD/YYYY'),
          '`source`': source,
          '`severity`': severity,
          '`reaction`': reaction,
          '`patients_id`': req.userData.userid
        })
        if (i === (multiple_condition_terms.length - 1)) {
          return res.json({
            message: 'Inserted Successfully',
            bool: true
          })
        }
      }
    }
    // const insertedID = await HealthModel.insert({
    //     "`type`": 'allergies',
    //     "`condition_term`": condition_term,
    //     "`date`": moment().tz(process.env.TZ).format('MM/DD/YYYY'),
    //     "`source`": source,
    //     "`severity`": severity,
    //     "`reaction`": reaction,
    //     "`patients_id`": req.userData.userid
    // });
    // const healthData = await HealthModel.getByID(insertedID);
    // res.json({
    //     message: 'Inserted Successfully',
    //     insertedData: healthData,
    //     bool: true
    // });
  } catch (e) {
    res.json(e)
  }
})

router.post('/bunch', async (req, res) => {
  try {
    const allergies = req.body.allergies || []
    for (let i = 0; i < allergies.length; i++) {
      await HealthModel.insert({
        '`type`': 'allergies',
        '`condition_term`': allergies[i].condition_term.trim(),
        '`date`': moment().tz(process.env.TZ).format('MM/DD/YYYY'),
        '`source`': 'Self Reported',
        '`severity`': allergies[i].symptom,
        '`reaction`': allergies[i].reaction,
        '`patients_id`': req.userData.userid,
      })
    }
    res.json({
      message: 'Inserted Successfully',
      bool: true
    })
  } catch (e) {
    res.json(e)
  }
})

router.put('/:healthID', async (req, res) => {
  try {
    await HealthModel.update(req.params.healthID, {
      condition_term: req.body.condition_term,
      severity: req.body.severity,
      reaction: req.body.reaction
    })
    res.json({
      message: 'Changes Saved',
      bool: true
    })
  } catch (e) {
    res.json(e)
  }
})

router.delete('/:healthID', async (req, res) => {
  try {
    const healthID = req.params.healthID
    await HealthModel.remove(healthID)
    res.json({
      message: 'Allergy deleted successfully.',
      bool: true
    })
  } catch (e) {
    res.json(e)
  }
})

module.exports = router