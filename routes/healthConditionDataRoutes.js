const express = require('express')
const router = express.Router()

// Models
const HealthConditionDataModel = require('../models/health_condition_data_tbl')

router.get('/', async (req, res) => {
  try {
    const healthConditionData = await HealthConditionDataModel.getAll()
    res.json({
      message: healthConditionData,
      bool: true
    })
  } catch (e) {
    res.json(e)
  }
})

module.exports = router