const express = require('express')
const router = express.Router()
const moment = require('moment-timezone')

// Models
const HealthModel = require('../models/health')

// Routes
const SurgeriesDataRoutes = require('./surgeriesDataRoutes')

router.use('/surgeries_data', SurgeriesDataRoutes)

router.get('/', async (req, res) => {
  try {
    const surgeriesData = await HealthModel.getData({
      patientID: req.userData.userid,
      type: 'surgery'
    })
    res.json({
      message: surgeriesData,
      bool: true
    })
  } catch (e) {
    res.json(e)
  }
})

router.post('/', async (req, res) => {
  try {
    const condition_term = req.body.condition_term.trim() || ''
    const source = req.body.source || 0
    if (condition_term == '' || source <= 0) {
      return res.json({
        message: 'Please fill all fields.',
        bool: false
      })
    }
    const insertedID = await HealthModel.insert({
      '`type`': 'surgery',
      '`condition_term`': condition_term,
      '`date`': moment().tz(process.env.TZ).format('MM/DD/YYYY'),
      '`source`': source,
      '`patients_id`': req.userData.userid
    })
    const healthData = await HealthModel.getByID(insertedID)
    res.json({
      message: 'Inserted Successfully',
      insertedData: healthData,
      bool: true
    })
  } catch (e) {
    res.json(e)
  }
})

router.post('/bunch', async (req, res) => {
  try {
    const surgeries = req.body.surgeries || []
    for (let i = 0; i < surgeries.length; i++) {
      await HealthModel.insert({
        '`type`': 'surgery',
        '`condition_term`': surgeries[i].condition_term.trim(),
        '`date`': moment().tz(process.env.TZ).format('MM/DD/YYYY'),
        '`source`': surgeries[i].source,
        '`patients_id`': req.userData.userid,
      })
    }
    res.json({
      message: 'Inserted Successfully',
      bool: true
    })
  } catch (e) {
    res.json(e)
  }
})

router.put('/:healthID', async (req, res) => {
  try {
    await HealthModel.update(req.params.healthID, {
      condition_term: req.body.condition_term,
      source: req.body.source
    })
    res.json({
      message: 'Changes Saved',
      bool: true
    })
  } catch (e) {
    res.json(e)
  }
})

router.delete('/:healthID', async (req, res) => {
  try {
    const healthID = req.params.healthID
    await HealthModel.remove(healthID)
    res.json({
      message: 'Surgery deleted successfully.',
      bool: true
    })
  } catch (e) {
    res.json(e)
  }
})

module.exports = router