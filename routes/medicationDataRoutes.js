const express = require('express')
const router = express.Router()

// Models
const MedicationDataModel = require('../models/medication_data_tbl')

router.get('/', async (req, res) => {
  try {
    const medicationData = await MedicationDataModel.getAll()
    res.json({
      message: medicationData,
      bool: true
    })
  } catch (e) {
    res.json(e)
  }
})

module.exports = router