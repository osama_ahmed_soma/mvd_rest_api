const express = require('express');
const router = express.Router();

// Models
const Reason = require('../models/reason');

router.get('/', async (req, res) => {
    try {
        const reasons = await Reason.getAll();
        res.send({
            message: '',
            reasons: reasons,
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});

module.exports = router;