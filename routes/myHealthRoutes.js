const express = require('express');
const router = express.Router();

// Models
const PatientModel = require('../models/patient');

router.get('/', async (req, res) => {
    try {
        const patientID = req.userData.userid;
        const patient = await PatientModel.getByUserID(patientID);
        res.json({
            message: {
                patient: {
                    is_health: patient.is_health,
                    is_allergies: patient.is_allergies,
                    is_medication: patient.is_medication,
                    is_surgeries: patient.is_surgeries
                }
            },
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});

router.put('/changeIsHealth', async (req, res) => {
    try {
        let isHealth = req.body.isHealth;
        if (isHealth) {
            isHealth = 2;
        } else {
            isHealth = 1;
        }
        await PatientModel.updateByUserID(req.userData.userid, {
            is_health: isHealth
        });
        res.json({
            message: '',
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});

router.put('/changeIsMedication', async (req, res) => {
    try {
        let isMedication = req.body.isMedication;
        if (isMedication) {
            isMedication = 2;
        } else {
            isMedication = 1;
        }
        await PatientModel.updateByUserID(req.userData.userid, {
            is_medication: isMedication
        });
        res.json({
            message: '',
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});

router.put('/changeIsAllergies', async (req, res) => {
    try {
        let isAllergies = req.body.isAllergies;
        if (isAllergies) {
            isAllergies = 2;
        } else {
            isAllergies = 1;
        }
        await PatientModel.updateByUserID(req.userData.userid, {
            is_allergies: isAllergies
        });
        res.json({
            message: '',
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});

router.put('/changeIsSurgeries', async (req, res) => {
    try {
        let isSurgeries = req.body.isSurgeries;
        if (isSurgeries) {
            isSurgeries = 2;
        } else {
            isSurgeries = 1;
        }
        await PatientModel.updateByUserID(req.userData.userid, {
            is_surgeries: isSurgeries
        });
        res.json({
            message: '',
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});

module.exports = router;