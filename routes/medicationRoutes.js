const express = require('express')
const router = express.Router()
const moment = require('moment-timezone')

// Models
const HealthModel = require('../models/health')

// Controller
const RxCutAPI = require('../controllers/rxCut-API')
const HealthOSAPI = require('../controllers/healthOS-API')

// Routes
const MedicationDataRoutes = require('./medicationDataRoutes')

router.use('/medication_data', MedicationDataRoutes)

router.get('/', async (req, res) => {
  try {
    const medicationData = await HealthModel.getData({
      patientID: req.userData.userid,
      type: 'medication'
    })
    res.json({
      message: medicationData,
      bool: true
    })
  } catch (e) {
    res.json(e)
  }
})

router.get('/getMedicines/:keyword', async (req, res) => {
  try {
    const keyword = req.params.keyword
    if (keyword.trim() == '') {
      return res.json({
        message: 'Please Add Medicine name.',
        bool: false
      })
    }
    let data = {
      full_data: {},
      full_data_array: [],
      names: []
    }
    if (keyword.trim().length >= 3) {
      // data = await RxCutAPI.getMedicine_api(keyword)
      data = await HealthOSAPI.getData(keyword.trim())
    }
    res.json({
      message: data,
      bool: true
    })
  } catch (e) {
    res.json(e)
  }
})

router.post('/', async (req, res) => {
  try {
    const condition_term = req.body.condition_term.trim() || ''
    const source = req.body.source.trim() || ''
    const dosage = req.body.dosage.trim() || ''
    const frequency = req.body.frequency.trim() || ''
    const time = req.body.time.trim() || ''
    if (condition_term == '') {
      return res.json({
        message: 'Please add Condition.',
        bool: false
      })
    }
    if (source == '') {
      return res.json({
        message: 'Please add Source.',
        bool: false
      })
    }
    if (dosage == '') {
      return res.json({
        message: 'Please add Dosage.',
        bool: false
      })
    }
    if (frequency == '') {
      return res.json({
        message: 'Please add Frequency.',
        bool: false
      })
    }
    if (time == '') {
      return res.json({
        message: 'Please add Time.',
        bool: false
      })
    }
    const insertedID = await HealthModel.insert({
      '`type`': 'medication',
      '`condition_term`': condition_term,
      '`date`': moment().tz(process.env.TZ).format('MM/DD/YYYY'),
      '`source`': source,
      '`dosage`': dosage,
      '`frequency`': frequency,
      '`time`': time,
      '`patients_id`': req.userData.userid
    })
    const healthData = await HealthModel.getByID(insertedID)
    res.json({
      message: 'Inserted Successfully',
      insertedData: healthData,
      bool: true
    })
  } catch (e) {
    res.json(e)
  }
})

router.post('/bunch', async (req, res) => {
  try {
    const medications = req.body.medications || []
    for (let i = 0; i < medications.length; i++) {
      await HealthModel.insert({
        '`type`': 'medication',
        '`condition_term`': medications[i].condition_term.trim(),
        '`date`': moment().tz(process.env.TZ).format('MM/DD/YYYY'),
        '`source`': 'Self Reported',
        '`dosage`': medications[i].dosage.trim(),
        '`frequency`': medications[i].frequency,
        '`time`': medications[i].time,
        '`patients_id`': req.userData.userid,
        '`medication_id`': medications[i].medication_id
      })
    }
    res.json({
      message: 'Inserted Successfully',
      bool: true
    })
  } catch (e) {
    res.json(e)
  }
})

router.put('/:healthID', async (req, res) => {
  try {
    await HealthModel.update(req.params.healthID, {
      condition_term: req.body.condition_term,
      dosage: req.body.dosage,
      frequency: req.body.frequency,
      time: req.body.time
    })
    res.json({
      message: 'Changes Saved',
      bool: true
    })
  } catch (e) {
    res.json(e)
  }
})

router.delete('/:healthID', async (req, res) => {
  try {
    const healthID = req.params.healthID
    await HealthModel.remove(healthID)
    res.json({
      message: 'Medication deleted successfully.',
      bool: true
    })
  } catch (e) {
    res.json(e)
  }
})

module.exports = router