const express = require('express');
const router = express.Router();
const config = require('../config/config');

const moment = require('moment-timezone');

// Middleware
const authorized = require('../middleware/authorized');
const masterData = require('../middleware/master-data');

// Routes
const UserRoutes = require('./userRoutes');
const DoctorRoutes = require('./doctorRoutes');
const PatientRoutes = require('./patientRoutes');
const AppointmentRoutes = require('./appointmentRoutes');
const MessageRoutes = require('./messageRoutes');
const ReasonRoutes = require('./reasonRoutes');
const WaiveFeeRoutes = require('./waiveFeeRoutes');
const ConsultationTypesRoutes = require('./consultationTypesRoutes');
const HealthRoutes = require('./healthRoutes');
const MedicationRoutes = require('./medicationRoutes');
const AllergiesRoutes = require('./allergiesRoutes');
const SurgeriesRoutes = require('./surgeriesRoutes');
const NotificationRoutes = require('./notificationRoutes');
const RegisteredDevicesRoutes = require('./registeredDevicesRoutes');

// Models
const State = require('../models/state');
const Specialty = require('../models/specialty');
const MessageModel = require('../models/message');
const AppointmentModel = require('../models/appointment');

// Controllers
const ProfileCompletionController = require('../controllers/profile-completion');

/* GET users listing. */
router.get('/', (req, res, next) => {
    res.send('This is a index route of this api');
});

router.use((req, res, next) => {
    console.log(process.env.TZ);
    console.log(moment().tz(process.env.TZ).format());
    next();
});

router.use('/user', UserRoutes);

router.get('/dashboard', authorized, masterData, async (req, res, next) => {
    try {
        // const profileCompletionData = await ProfileCompletionController.getProfileCompletion(req.userData.userid);
        const states = await State.getAll();
        const specialties = await Specialty.getAll();

        // get number of inbox messages
        const totalInbox = await MessageModel.getInbox(req.userData.userid);
        const totalUpcomingAppointment = await AppointmentModel.getUpcoming(req.userData.userid);
        const totalAppointment = await AppointmentModel.getAll(req.userData.userid);
        // get total upcoming appointment

        res.json({
            message: {
                inbox: {
                    unread: req.unread_messages,
                    total: totalInbox.length
                }
            },
            appointment: {
                upcoming: totalUpcomingAppointment.length,
                total: totalAppointment.length
            },
            unread_notifications: req.unread_notifications,
            userData: req.userData,
            // profileCompletionData: profileCompletionData,
            states: states,
            specialties: specialties,
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});

router.use('/patient', authorized, PatientRoutes);

router.use('/appointment', authorized, AppointmentRoutes);

router.use('/doctor', authorized, DoctorRoutes);

router.use('/message', authorized, MessageRoutes);

router.use('/reason', authorized, ReasonRoutes);

router.use('/waive_fee', authorized, WaiveFeeRoutes);

router.use('/consultation_type', authorized, ConsultationTypesRoutes);

router.use('/health', authorized, HealthRoutes);

router.use('/medication', authorized, MedicationRoutes);

router.use('/allergies', authorized, AllergiesRoutes);

router.use('/surgeries', authorized, SurgeriesRoutes);

router.use('/notification', authorized, NotificationRoutes);

router.use('/registered_devices', authorized, RegisteredDevicesRoutes);

module.exports = router;