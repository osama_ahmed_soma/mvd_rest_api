const express = require('express')
const router = express.Router()
const moment = require('moment-timezone')

// Routes
const HealthConditionDataRoutes = require('./healthConditionDataRoutes')

// Models
const HealthModel = require('../models/health')

router.use('/health_condition_data', HealthConditionDataRoutes)

router.get('/', async (req, res) => {
  try {
    const healthData = await HealthModel.getData({
      patientID: req.userData.userid,
      type: 'health'
    })
    res.json({
      message: healthData,
      bool: true
    })
  } catch (e) {
    res.json(e)
  }
})

router.post('/', async (req, res) => {
  try {
    const condition_term = req.body.condition.trim() || ''
    const source = req.body.source.trim() || ''
    if (condition_term == '') {
      return res.json({
        message: 'Please add Condition.',
        bool: false
      })
    }
    if (source == '') {
      return res.json({
        message: 'Please add Source.',
        bool: false
      })
    }
    const insertedID = await HealthModel.insert({
      '`type`': 'health',
      '`condition_term`': condition_term,
      '`date`': moment().tz(process.env.TZ).format('MM/DD/YYYY'),
      '`source`': source,
      '`patients_id`': req.userData.userid
    })
    const healthData = await HealthModel.getByID(insertedID)
    res.json({
      message: 'Inserted Successfully',
      insertedData: healthData,
      bool: true
    })
  } catch (e) {
    res.json(e)
  }
})

router.post('/bunch', async (req, res) => {
  try {
    const conditions = req.body.conditions || []
    for (let i = 0; i < conditions.length; i++) {
      await HealthModel.insert({
        '`type`': 'health',
        '`condition_term`': conditions[i].trim(),
        '`date`': moment().tz(process.env.TZ).format('MM/DD/YYYY'),
        '`source`': 'Self Reported',
        '`patients_id`': req.userData.userid
      })
    }
    res.json({
      message: 'Inserted Successfully',
      bool: true
    })
  } catch (e) {
    res.json(e)
  }
})

router.put('/:healthID', async (req, res) => {
  try {
    await HealthModel.update(req.params.healthID, {
      condition_term: req.body.condition
    })
    res.json({
      message: 'Changes Saved',
      bool: true
    })
  } catch (e) {
    res.json(e)
  }
})

router.delete('/:healthID', async (req, res) => {
  try {
    const healthID = req.params.healthID
    await HealthModel.remove(healthID)
    res.json({
      message: 'Health condition deleted successfully.',
      bool: true
    })
  } catch (e) {
    res.json(e)
  }
})

module.exports = router