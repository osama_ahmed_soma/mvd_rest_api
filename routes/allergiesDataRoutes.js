const express = require('express')
const router = express.Router()

// Models
const AllergiesDataModel = require('../models/allergies_data_tbl')

router.get('/', async (req, res) => {
  try {
    const allergiesData = await AllergiesDataModel.getAll()
    res.json({
      message: allergiesData,
      bool: true
    })
  } catch (e) {
    res.json(e)
  }
})

module.exports = router