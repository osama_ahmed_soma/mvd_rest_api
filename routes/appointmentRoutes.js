const express = require('express');
const router = express.Router();
const moment = require('moment-timezone');

// Models
const AppointmentModel = require('../models/appointment');
const ReasonModel = require('../models/reason');
const AppointmentLogModel = require('../models/appointment_log');
const AppointmentKnockingDataModel = require('../models/appointment_knocking_data');
const PatientModel = require('../models/patient');

// Config
const config = require('../config/config');

// Provider
const OpentokProvider = require('../providers/opentok.provider');

router.get('/upcoming/:page*?', async (req, res) => {
    try {
        const appointments = await AppointmentModel.getUpcoming(req.userData.userid, req.params.page);
        res.json({
            appointments: appointments,
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});
router.get('/pending', async (req, res) => {
    try {
        const appointments = await AppointmentModel.getPending(req.userData.userid);
        res.json(appointments);
    } catch (e) {
        res.json(e);
    }
});
router.get('/cancelled/:page*?', async (req, res) => {
    try {
        const appointments = await AppointmentModel.getCancelled(req.userData.userid, req.params.page);
        res.json({
            appointments: appointments,
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});
router.get('/past/:page*?', async (req, res) => {
    try {
        const appointments = await AppointmentModel.getPast(req.userData.userid, req.params.page);
        res.json({
            appointments: appointments,
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});
router.post('/schedule', async (req, res) => {
    try {
        req.body.patientID = req.userData.userid;
        req.body.ipAddress = req.ip;
        await AppointmentModel.schedule(req.body);
        res.json({
            message: 'Congratulations! Schedule is successfully placed.',
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});
router.get('/patientKnocked/:appointmentID', async (req, res) => {
    try {
        const appointmentID = req.params.appointmentID;
        await AppointmentKnockingDataModel.updateByAppointmentID(appointmentID, {
            is_patient_approved: 1
        });
        res.json({
            message: 'You successfully knocked. Please wait your doctor will start consultation shortly.',
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});
router.get('/startVideo/:appointmentID', async (req, res) => {
    try {
        const patientID = req.userData.userid;
        // const patientID = 1;
        const appointmentID = req.params.appointmentID;
        if (!appointmentID) {
            return res.json({
                message: 'Wrong action.',
                bool: false
            });
        }

        const app = await AppointmentModel.getByID(appointmentID);

        if (app.patients_id != patientID) {
            return res.json({
                message: 'The action you have requested is not allowed.',
                bool: false
            });
        }

        if (!app.tokbox_session) {
            const sessionID = await OpentokProvider.generateSession();
            await AppointmentModel.updateByID(appointmentID, {
                tokbox_session: sessionID
            });
            app.tokbox_session = sessionID;
        }
        const reason = await ReasonModel.getByID(app.reason_id);
        app.reason_name = reason.name;
        app.durationReadable = '';
        const token = OpentokProvider.generateToken(app.tokbox_session);


        const appointmentLog = await AppointmentLogModel.getByAppointmentID(appointmentID);

        // update doctor that patient is in the video page.

        // get patient timestamp
        const patientData = await PatientModel.getByUserID(patientID);

        const currentTimeStamp = moment().tz(patientData.timezone).unix();
        const thirtyMinutesTimeStamp = moment().tz(patientData.timezone).add(30, 'minutes').unix();

        // update appointment time too
        await AppointmentModel.updateByID(appointmentID, {
            start_date_stamp: currentTimeStamp,
            end_date_stamp: thirtyMinutesTimeStamp
        });



        await AppointmentLogModel.updateByAppointmentID(appointmentID, {
            session_start_stamp: currentTimeStamp
        });

        res.json({
            message: '',
            result: {
                apiKey: config.tokenBox.apiKey,
                appointment: app,
                openTokToken: token,
                appointmentLog: appointmentLog
            },
            bool: true
        });

    } catch (e) {
        res.json(e);
    }
});

module.exports = router;