const express = require('express');
const router = express.Router();

// Models
const UserRegisteredDevicesModel = require('../models/user_registeredDevice');

router.get('/:deviceID', async (req, res) => {
    try {
        const deviceID = req.params.deviceID;
        const deviceData = await UserRegisteredDevicesModel.getByDeviceID(deviceID);
        res.json({
            message: '',
            data: deviceData,
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});

router.put('/change', async (req, res) => {
    try {
        const deviceID = req.body.deviceID;
        const messageFlag = req.body.messageFlag;
        const type = req.body.type;
        let data;
        if(type === 'message') {
            data = {
                messageNotify: messageFlag
            };
        } else {
            data = {
                appointmentNotify: messageFlag
            };
        }
        await UserRegisteredDevicesModel.updateByDeviceID(data, deviceID);
        res.json({
            message: '',
            bool: true
        });
    } catch (e) {
        res.json(e);
    }
});

module.exports = router;