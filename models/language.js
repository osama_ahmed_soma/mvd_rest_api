const pool = require('../config/dbConnection');
const language = {};
language.getByID = langID => {
    return new Promise((resolve, reject) => {
        if (!langID) {
            return reject({
                message: 'Please provide Language ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT `name` FROM `language` WHERE `id` = ?", [langID], async (err, language) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve(language);
            });
        });
    });
};
module.exports = language;