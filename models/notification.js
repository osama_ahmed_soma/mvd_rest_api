const moment = require('moment-timezone');
const config = require('../config/config');
const pool = require('../config/dbConnection');
const User = require('./user');
const obj = {};

obj.add = data => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            const allKeysArray = Object.keys(data);
            const allValuesArray = Object.values(data);
            const allKeysString = '`' + allKeysArray.join('`, `') + '`';
            connection.query("INSERT INTO `notification` (" + allKeysString + ") VALUES (" + ('?, '.repeat(allKeysArray.length).replace(/,\s*$/, "")) + ")", allValuesArray, async (err, result) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve(result.insertId);
            });
            // connection.query("INSERT INTO `notification` (`from_userid`, `to_userid`, `from_usertype`, `to_usertype`, `message`, `type`, `added`) VALUES (?, ?, ?, ?, ?, ?, ?)", [
            //     data.from_userid,
            //     data.to_userid,
            //     data.from_usertype,
            //     data.to_usertype,
            //     data.message,
            //     data.type,
            //     data.added
            // ], async (err, result) => {
            //     connection.release();
            //     if (err) {
            //         return reject({
            //             message: 'There is an error while querying to the database. Please try again.',
            //             bool: false
            //         });
            //     }
            //     resolve(result.insertId);
            // });
        });
    });
};

obj.getAll = userID => {
    return new Promise(async (resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide User ID.',
                bool: false
            });
        }
        try {
            const isUserExist = await User.isExists(userID);
            if (!isUserExist) {
                return reject({
                    message: 'There is no User exist with the ID#: ' + userID,
                    bool: false
                });
            }
            pool.getConnection((err, connection) => {
                if (err) {
                    // connection.release();
                    return reject({
                        message: 'There is an error while connecting to the database. Please try again.',
                        bool: false
                    });
                }
                connection.query("SELECT `notification`.`id`, `notification`.`subject`, `notification`.`message`, `notification`.`type`, `notification`.`appointmentID`, `notification`.`added`, `doctor`.`image_url`, `doctor`.`firstname`, `doctor`.`lastname` FROM `notification` JOIN `doctor` ON `notification`.`from_userid` = `doctor`.`userid` WHERE `notification`.`to_userid` = ? AND `notification`.`view` = 0 AND `notification`.`deleted` = 0 ORDER BY `notification`.`id` DESC LIMIT 10", [
                    userID
                ], (err, results) => {
                    connection.release();
                    if (err) {
                        return reject({
                            message: err,
                            bool: false
                        });
                    }
                    if (results.length > 0) {
                        for (let i = 0; i < results.length; i++) {
                            let localMessage = results[i].message.substring(0, results[i].message.indexOf('for'));
                            if (results[i].appointmentID !== null) {
                                localMessage = results[i].message;
                            }
                            results[i].message = localMessage;
                            results[i].added = moment.tz((results[i].added * 1000), process.env.TZ).format('MM/DD/YYYY hh:mm A');
                            results[i].image_url = config.originalServer + '' + results[i].image_url;
                            if (i == (results.length - 1)) {
                                // resolve here
                                resolve(results);
                            }
                        }
                    } else {
                        resolve(results);
                    }
                });
            });
        } catch (e) {
            return reject(e);
        }
    });
};

obj.getUnreadCount = userID => {
    return new Promise(async (resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide User ID.',
                bool: false
            });
        }
        try {
            const isUserExist = await User.isExists(userID);
            if (!isUserExist) {
                return reject({
                    message: 'There is no User exist with the ID#: ' + userID,
                    bool: false
                });
            }
            pool.getConnection((err, connection) => {
                if (err) {
                    // connection.release();
                    return reject({
                        message: 'There is an error while connecting to the database. Please try again.',
                        bool: false
                    });
                }
                connection.query("SELECT COUNT(*) AS `numrows` FROM `notification` WHERE `to_userid` = ? AND `view` =0 AND `deleted` =0", [
                    userID
                ], (err, results) => {
                    connection.release();
                    if (err) {
                        return reject({
                            message: err,
                            bool: false
                        });
                    }
                    resolve(results[0].numrows);
                });
            });
        } catch (e) {
            return reject(e);
        }
    });
};
obj.getByAppointmentIDAndType = (appointmentID, type) => {
    return new Promise((resolve, reject) => {
        if (!appointmentID) {
            return reject({
                message: 'Please provide User ID.',
                bool: false
            });
        }
        if (!type) {
            return reject({
                message: 'Notification type is not defined.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `notification` WHERE `notification`.`appointmentID` = ? AND `notification`.`type` = ? LIMIT 1", [
                appointmentID,
                type
            ], (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: err,
                        bool: false
                    });
                }
                if (results.length > 0) {
                    resolve(results[0]);
                } else {
                    resolve(false);
                }
            });
        });

    });
};
obj.updateByUserIDAndType = (userID, type, data) => {
    return new Promise(async (resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide User ID.',
                bool: false
            });
        }
        if (!type) {
            return reject({
                message: 'User\'s type is not defined.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("UPDATE `notification` SET ? WHERE `to_userID` = ? AND `to_usertype` = ?", [
                data,
                userID,
                type
            ], (err) => {
                connection.release();
                if (err) {
                    return reject({
                        message: err,
                        bool: false
                    });
                }
                resolve();
            });
        });
    });
};
obj.updateByID = (notificationID, data) => {
    return new Promise(async (resolve, reject) => {
        if (!notificationID) {
            return reject({
                message: 'Please provide Notification ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("UPDATE `notification` SET ? WHERE `id` = ?", [
                data,
                notificationID
            ], (err) => {
                connection.release();
                if (err) {
                    return reject({
                        message: err,
                        bool: false
                    });
                }
                resolve();
            });
        });
    });
};
module.exports = obj;