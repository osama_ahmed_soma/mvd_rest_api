const pool = require('../config/dbConnection')
const medication_data_tbl = {}

medication_data_tbl.getAll = () => {
  return new Promise((resolve, reject) => {
    pool.getConnection((err, connection) => {
      if (err) {
        return reject({
          message: 'There is an error while connecting to the database. Please try again.',
          bool: false
        })
      }
      connection.query('SELECT * FROM `medication_data_tbl`', [], (err, results) => {
        connection.release()
        if (err) {
          return reject({
            message: 'There is an error while querying to the database. Please try again.',
            bool: false
          })
        }
        resolve(results)
      })
    })
  })
}

module.exports = medication_data_tbl