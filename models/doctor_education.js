const pool = require('../config/dbConnection');
const doctor_education = {};
doctor_education.getByUserID = userID => {
    return new Promise((resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide Doctor ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `doctor_education` WHERE `userid` = ?", [userID], (err, education) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve(education);
            });
        });
    });
};
module.exports = doctor_education;