const moment = require('moment-timezone');
const pool = require('../config/dbConnection');

// Config
const config = require('../config/config');

// Models
const DoctorModel = require('./doctor');
const PatientModel = require('./patient');


const User = require('./user');
const obj = {};

obj.insert = data => {
    return new Promise(async (resolve, reject) => {
        if (!data.patientID) {
            return reject({
                message: 'Please provide Patient ID.',
                bool: false
            });
        }
        if (!data.recipientID) {
            return reject({
                message: 'Please provide Recipient ID.',
                bool: false
            });
        }
        if (!data.subject) {
            return reject({
                message: 'You must provide a Subject.',
                bool: false
            });
        }
        if (!data.message) {
            return reject({
                message: 'The Message field is required.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("INSERT INTO `messages` (`from_userid`, `to_userid`, `parent`, `subject`, `message`, `added`, `draft`) VALUES (?, ?, ?, ?, ?, ?, ?)", [
                data.patientID,
                data.recipientID,
                0,
                data.subject,
                data.message,
                moment.tz((new Date()).getTime(), process.env.TZ).unix(),
                0
            ], (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        error: err,
                        bool: false
                    });
                }
                resolve(results.insertId);
            });
        });
    });
};

obj.getByID = messageID => {
    return new Promise(async (resolve, reject) => {
        if (!messageID) {
            return reject({
                message: 'Please provide Message ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `messages` WHERE `id` = ? LIMIT 1", [
                messageID
            ], (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        error: err,
                        bool: false
                    });
                }
                resolve(results);
            });
        });
    });
};
obj.getUnread = (userID) => {
    return new Promise(async (resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide User ID.',
                bool: false
            });
        }
        try {
            const isUserExist = await User.isExists(userID);
            if (!isUserExist) {
                return reject({
                    message: 'There is no User exist with the ID#: ' + userID,
                    bool: false
                });
            }
            pool.getConnection((err, connection) => {
                if (err) {
                    // connection.release();
                    return reject({
                        message: 'There is an error while connecting to the database. Please try again.',
                        bool: false
                    });
                }
                connection.query("SELECT * FROM `messages` WHERE `to_userid` = ? AND `draft` = 0 AND `recipient_delete` = 0 AND `opened` = 0", [userID], (err, results) => {
                    connection.release();
                    if (err) {
                        return reject({
                            message: 'There is an error while querying to the database. Please try again.',
                            error: err,
                            bool: false
                        });
                    }
                    resolve(results);
                });
            });
        } catch (e) {
            return reject(e);
        }
    });
};
obj.getUnreadCount = (userID) => {
    return new Promise(async (resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide User ID.',
                bool: false
            });
        }
        try {
            const isUserExist = await User.isExists(userID);
            if (!isUserExist) {
                return reject({
                    message: 'There is no User exist with the ID#: ' + userID,
                    bool: false
                });
            }
            pool.getConnection((err, connection) => {
                if (err) {
                    // connection.release();
                    return reject({
                        message: 'There is an error while connecting to the database. Please try again.',
                        bool: false
                    });
                }
                connection.query("SELECT * FROM `messages` WHERE `to_userid` = ? AND `draft` = 0 AND `recipient_delete` = 0 AND `opened` = 0", [userID], (err, results) => {
                    connection.release();
                    if (err) {
                        return reject({
                            message: 'There is an error while querying to the database. Please try again.',
                            error: err,
                            bool: false
                        });
                    }
                    resolve(results.length);
                });
            });
        } catch (e) {
            return reject(e);
        }
    });
};
obj.getInbox = userID => {
    return new Promise(async (resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide User ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT `msg`.`id`, `msg`.`subject`, `msg`.`from_userid`, `msg`.`message`, `msg`.`opened`, `msg`.`added`, `usr`.`firstname`, `usr`.`lastname`, `usr`.`image_url`, `usr`.`city`, `st`.`code` FROM `messages` `msg` JOIN `doctor` `usr` ON `usr`.`userid` = `msg`.`from_userid` LEFT JOIN `state` `st` ON `st`.`id` = `usr`.`state_id` WHERE `msg`.`to_userid` = ? AND `msg`.`draft` = '0' AND `msg`.`recipient_delete` = '0' ORDER BY `msg`.`id` DESC", [userID], (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        error: err,
                        bool: false
                    });
                }
                if (results.length > 0) {
                    for (let i = 0; i < results.length; i++) {
                        results[i].added = moment.unix(results[i].added).tz(process.env.TZ).fromNow();
                        if (results[i].image_url) {
                            results[i].image_url = config.originalServer + '' + results[i].image_url;
                        }
                        if (i == (results.length - 1)) {
                            resolve(results);
                        }
                    }
                } else {
                    resolve(results);
                }
            });
        });
    });
};
obj.getSent = userID => {
    return new Promise(async (resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide User ID.',
                bool: false
            });
        }
        //    SELECT `msg`.`id`, `msg`.`subject`, `msg`.`to_userid`, `msg`.`message`, `msg`.`opened`, `msg`.`added`, `usr`.`firstname`, `usr`.`lastname`, `usr`.`image_url`, `usr`.`city`, `st`.`code` FROM `messages` `msg` JOIN `doctor` `usr` ON `usr`.`userid` = `msg`.`to_userid` LEFT JOIN `state` `st` ON `st`.`id` = `usr`.`state_id` WHERE `msg`.`from_userid` = 1 AND `msg`.`draft` = '0' AND `msg`.`sender_delete` = '0' ORDER BY `msg`.`id` DESC
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT `msg`.`id`, `msg`.`subject`, `msg`.`to_userid`, `msg`.`message`, `msg`.`opened`, `msg`.`added`, `usr`.`firstname`, `usr`.`lastname`, `usr`.`image_url`, `usr`.`city`, `st`.`code` FROM `messages` `msg` JOIN `doctor` `usr` ON `usr`.`userid` = `msg`.`to_userid` LEFT JOIN `state` `st` ON `st`.`id` = `usr`.`state_id` WHERE `msg`.`from_userid` = ? AND `msg`.`draft` = '0' AND `msg`.`sender_delete` = '0' ORDER BY `msg`.`id` DESC", [userID], (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        error: err,
                        bool: false
                    });
                }
                if (results.length > 0) {
                    for (let i = 0; i < results.length; i++) {
                        results[i].added = moment.unix(results[i].added).tz(process.env.TZ).fromNow();
                        if (results[i].image_url) {
                            results[i].image_url = config.originalServer + '' + results[i].image_url;
                        }
                        if (i == (results.length - 1)) {
                            resolve(results);
                        }
                    }
                } else {
                    resolve(results);
                }
            });
        });
    });
};
obj.getDraft = userID => {
    return new Promise(async (resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide User ID.',
                bool: false
            });
        }
        //    SELECT `msg`.`id`, `msg`.`subject`, `msg`.`to_userid`, `msg`.`message`, `msg`.`opened`, `msg`.`added`, `usr`.`firstname`, `usr`.`lastname`, `usr`.`image_url`, `usr`.`city`, `st`.`code` FROM `messages` `msg` JOIN `patient` `usr` ON `usr`.`user_id` = `msg`.`from_userid` LEFT JOIN `state` `st` ON `st`.`id` = `usr`.`state_id` WHERE `msg`.`from_userid` = 1 AND `msg`.`draft` = '1' AND `msg`.`sender_delete` = '0' ORDER BY `msg`.`id` DESC
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT `msg`.`id`, `msg`.`subject`, `msg`.`to_userid`, `msg`.`message`, `msg`.`opened`, `msg`.`added`, `usr`.`firstname`, `usr`.`lastname`, `usr`.`image_url`, `usr`.`city`, `st`.`code` FROM `messages` `msg` JOIN `patient` `usr` ON `usr`.`user_id` = `msg`.`from_userid` LEFT JOIN `state` `st` ON `st`.`id` = `usr`.`state_id` WHERE `msg`.`from_userid` = ? AND `msg`.`draft` = '1' AND `msg`.`sender_delete` = '0' ORDER BY `msg`.`id` DESC", [userID], (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        error: err,
                        bool: false
                    });
                }
                if (results.length > 0) {
                    for (let i = 0; i < results.length; i++) {
                        results[i].added = moment.unix(results[i].added).tz(process.env.TZ).fromNow();
                        if (results[i].image_url) {
                            results[i].image_url = config.originalServer + '' + results[i].image_url;
                        }
                        if (i == (results.length - 1)) {
                            resolve(results);
                        }
                    }
                } else {
                    resolve(results);
                }
            });
        });
    });
};
obj.getTrash = userID => {
    return new Promise(async (resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide User ID.',
                bool: false
            });
        }
        //    SELECT `msg`.`id`, `msg`.`subject`, `msg`.`to_userid`, `msg`.`from_userid`, `msg`.`message`, `msg`.`opened`, `msg`.`added` FROM `messages` as `msg` WHERE (`msg`.`from_userid` = 1 AND `msg`.`sender_delete` = 1) OR (`msg`.`to_userid` = 1 and `msg`.`recipient_delete` = 1) ORDER BY `msg`.`updated` DESC LIMIT 10
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT `msg`.`id`, `msg`.`subject`, `msg`.`to_userid`, `msg`.`from_userid`, `msg`.`message`, `msg`.`opened`, `msg`.`added` FROM `messages` as `msg` WHERE (`msg`.`from_userid` = ? AND `msg`.`sender_delete` = 1) OR (`msg`.`to_userid` = ? and `msg`.`recipient_delete` = 1) ORDER BY `msg`.`updated` DESC", [
                userID,
                userID
            ], async (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        error: err,
                        bool: false
                    });
                }
                if (results.length > 0) {
                    try {
                        for (let i = 0; i < results.length; i++) {
                            results[i].added = moment.unix(results[i].added).tz(process.env.TZ).fromNow();
                            let doctor = await DoctorModel.getByUserID(results[i].from_userid, '*', true);
                            if (doctor.length > 0) {
                                doctor = doctor[0];
                                results[i].firstname = doctor.firstname;
                                results[i].lastname = doctor.lastname;
                                results[i].image_url = '';
                                if (doctor.image_url) {
                                    results[i].image_url = config.originalServer + '' + doctor.image_url;
                                }
                            } else {
                                let patient = await PatientModel.getByUserID(results[i].from_userid, '*', true);
                                if (patient.length > 0) {
                                    patient = patient[0];
                                    results[i].firstname = patient.firstname;
                                    results[i].lastname = patient.lastname;
                                    results[i].image_url = '';
                                    if (patient.image_url) {
                                        results[i].image_url = config.originalServer + '' + patient.image_url;
                                    }
                                }
                            }
                            if (i == (results.length - 1)) {
                                resolve(results);
                            }
                        }
                    } catch (e) {
                        reject(e);
                    }
                } else {
                    resolve(results);
                }
            });
        });
    });
};
obj.searchInboxMessages = (searchText, patientID) => {
    return new Promise(async (resolve, reject) => {
        if (searchText == '') {
            return reject({
                message: 'Search field is empty.',
                bool: false
            });
        }
        if (!patientID) {
            return reject({
                message: 'Please provide Patient ID.',
                bool: false
            });
        }
        // SELECT `msg`.`id`, `msg`.`subject`, `msg`.`from_userid`, `msg`.`message`, `msg`.`opened`, `msg`.`added`, `usr`.`firstname`, `usr`.`lastname`, `usr`.`image_url`, `usr`.`city`, `st`.`code` FROM `messages` `msg` JOIN `doctor` `usr` ON `usr`.`userid` = `msg`.`from_userid` LEFT JOIN `state` `st` ON `st`.`id` = `usr`.`state_id` WHERE `msg`.`to_userid` = '1' AND `msg`.`draft` = '0' AND `msg`.`recipient_delete` = '0' AND (`msg`.`subject` LIKE '%ontact%' ESCAPE '!' OR `msg`.`message` LIKE '%ontact%' ESCAPE '!' ) ORDER BY `msg`.`id` DESC LIMIT 10
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT `msg`.`id`, `msg`.`subject`, `msg`.`from_userid`, `msg`.`message`, `msg`.`opened`, `msg`.`added`, `usr`.`firstname`, `usr`.`lastname`, `usr`.`image_url`, `usr`.`city`, `st`.`code` FROM `messages` `msg` JOIN `doctor` `usr` ON `usr`.`userid` = `msg`.`from_userid` LEFT JOIN `state` `st` ON `st`.`id` = `usr`.`state_id` WHERE `msg`.`to_userid` = ? AND `msg`.`draft` = '0' AND `msg`.`recipient_delete` = '0' AND (`msg`.`subject` LIKE ? ESCAPE '!' OR `msg`.`message` LIKE ? ESCAPE '!' ) ORDER BY `msg`.`id` DESC", [
                patientID,
                '%' + searchText + '%',
                '%' + searchText + '%'
            ], (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        error: err,
                        bool: false
                    });
                }
                if (results.length > 0) {
                    for (let i = 0; i < results.length; i++) {
                        results[i].added = moment.unix(results[i].added).tz(process.env.TZ).fromNow();
                        if (results[i].image_url) {
                            results[i].image_url = config.originalServer + '' + results[i].image_url;
                        }
                        if (i == (results.length - 1)) {
                            resolve(results);
                        }
                    }
                } else {
                    resolve(results);
                }
            });
        });
    });
};
obj.searchSentMessages = (searchText, patientID) => {
    return new Promise(async (resolve, reject) => {
        if (searchText == '') {
            return reject({
                message: 'Search field is empty.',
                bool: false
            });
        }
        if (!patientID) {
            return reject({
                message: 'Please provide Patient ID.',
                bool: false
            });
        }
        // SELECT `msg`.`id`, `msg`.`subject`, `msg`.`from_userid`, `msg`.`message`, `msg`.`opened`, `msg`.`added`, `usr`.`firstname`, `usr`.`lastname`, `usr`.`image_url`, `usr`.`city`, `st`.`code` FROM `messages` `msg` JOIN `patient` `usr` ON `usr`.`user_id` = `msg`.`from_userid` LEFT JOIN `state` `st` ON `st`.`id` = `usr`.`state_id` WHERE `msg`.`from_userid` = '1' AND `msg`.`draft` = '0' AND `msg`.`sender_delete` = '0' AND (`msg`.`subject` LIKE '%contact%' ESCAPE '!' OR `msg`.`message` LIKE '%contact%' ESCAPE '!' ) ORDER BY `msg`.`id` DESC LIMIT 10
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT `msg`.`id`, `msg`.`subject`, `msg`.`from_userid`, `msg`.`message`, `msg`.`opened`, `msg`.`added`, `usr`.`firstname`, `usr`.`lastname`, `usr`.`image_url`, `usr`.`city`, `st`.`code` FROM `messages` `msg` JOIN `patient` `usr` ON `usr`.`user_id` = `msg`.`from_userid` LEFT JOIN `state` `st` ON `st`.`id` = `usr`.`state_id` WHERE `msg`.`from_userid` = ? AND `msg`.`draft` = '0' AND `msg`.`sender_delete` = '0' AND (`msg`.`subject` LIKE ? ESCAPE '!' OR `msg`.`message` LIKE ? ESCAPE '!' ) ORDER BY `msg`.`id` DESC", [
                patientID,
                '%' + searchText + '%',
                '%' + searchText + '%'
            ], (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        error: err,
                        bool: false
                    });
                }
                if (results.length > 0) {
                    for (let i = 0; i < results.length; i++) {
                        results[i].added = moment.unix(results[i].added).tz(process.env.TZ).fromNow();
                        if (results[i].image_url) {
                            results[i].image_url = config.originalServer + '' + results[i].image_url;
                        }
                        if (i == (results.length - 1)) {
                            resolve(results);
                        }
                    }
                } else {
                    resolve(results);
                }
            });
        });
    });
};
obj.searchDraftMessages = (searchText, patientID) => {
    return new Promise(async (resolve, reject) => {
        if (searchText == '') {
            return reject({
                message: 'Search field is empty.',
                bool: false
            });
        }
        if (!patientID) {
            return reject({
                message: 'Please provide Patient ID.',
                bool: false
            });
        }
        // SELECT `msg`.`id`, `msg`.`subject`, `msg`.`to_userid`, `msg`.`message`, `msg`.`opened`, `msg`.`added`, `usr`.`firstname`, `usr`.`lastname`, `usr`.`image_url`, `usr`.`city`, `st`.`code` FROM `messages` `msg` JOIN `patient` `usr` ON `usr`.`user_id` = `msg`.`from_userid` LEFT JOIN `state` `st` ON `st`.`id` = `usr`.`state_id` WHERE `msg`.`from_userid` = '1' AND `msg`.`draft` = '1' AND `msg`.`sender_delete` = '0' AND (`msg`.`subject` LIKE '%a%' ESCAPE '!' OR `msg`.`message` LIKE '%a%' ESCAPE '!' ) ORDER BY `msg`.`id` DESC LIMIT 10
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT `msg`.`id`, `msg`.`subject`, `msg`.`to_userid`, `msg`.`message`, `msg`.`opened`, `msg`.`added`, `usr`.`firstname`, `usr`.`lastname`, `usr`.`image_url`, `usr`.`city`, `st`.`code` FROM `messages` `msg` JOIN `patient` `usr` ON `usr`.`user_id` = `msg`.`from_userid` LEFT JOIN `state` `st` ON `st`.`id` = `usr`.`state_id` WHERE `msg`.`from_userid` = ? AND `msg`.`draft` = '1' AND `msg`.`sender_delete` = '0' AND (`msg`.`subject` LIKE ? ESCAPE '!' OR `msg`.`message` LIKE ? ESCAPE '!' ) ORDER BY `msg`.`id` DESC", [
                patientID,
                '%' + searchText + '%',
                '%' + searchText + '%'
            ], (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        error: err,
                        bool: false
                    });
                }
                if (results.length > 0) {
                    for (let i = 0; i < results.length; i++) {
                        results[i].added = moment.unix(results[i].added).tz(process.env.TZ).fromNow();
                        if (results[i].image_url) {
                            results[i].image_url = config.originalServer + '' + results[i].image_url;
                        }
                        if (i == (results.length - 1)) {
                            resolve(results);
                        }
                    }
                } else {
                    resolve(results);
                }
            });
        });
    });
};
obj.searchTrashMessages = (searchText, patientID) => {
    return new Promise(async (resolve, reject) => {
        if (searchText == '') {
            return reject({
                message: 'Search field is empty.',
                bool: false
            });
        }
        if (!patientID) {
            return reject({
                message: 'Please provide Patient ID.',
                bool: false
            });
        }
        // SELECT `msg`.`id`, `msg`.`subject`, `msg`.`to_userid`, `msg`.`from_userid`, `msg`.`message`, `msg`.`opened`, `msg`.`added` FROM `messages` `msg` WHERE (`msg`.`subject` LIKE '%test%' ESCAPE '!' OR `msg`.`message` LIKE '%test%' ESCAPE '!' ) AND ((msg.from_userid = 1 AND `msg`.`sender_delete` = 1) OR (`msg`.`to_userid` = 1 and msg.recipient_delete = 1)) ORDER BY `msg`.`updated` DESC LIMIT 10
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT `msg`.`id`, `msg`.`subject`, `msg`.`to_userid`, `msg`.`from_userid`, `msg`.`message`, `msg`.`opened`, `msg`.`added` FROM `messages` `msg` WHERE (`msg`.`subject` LIKE ? ESCAPE '!' OR `msg`.`message` LIKE ? ESCAPE '!' ) AND ((msg.from_userid = ? AND `msg`.`sender_delete` = 1) OR (`msg`.`to_userid` = ? and msg.recipient_delete = 1)) ORDER BY `msg`.`updated` DESC", [
                '%' + searchText + '%',
                '%' + searchText + '%',
                patientID,
                patientID
            ], async (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        error: err,
                        bool: false
                    });
                }
                if (results.length > 0) {
                    for (let i = 0; i < results.length; i++) {
                        results[i].added = moment.unix(results[i].added).tz(process.env.TZ).fromNow();
                        let doctor = await DoctorModel.getByUserID(results[i].from_userid, '*', true);
                        if (doctor.length > 0) {
                            doctor = doctor[0];
                            results[i].firstname = doctor.firstname;
                            results[i].lastname = doctor.lastname;
                            results[i].image_url = '';
                            if (doctor.image_url) {
                                results[i].image_url = config.originalServer + '' + doctor.image_url;
                            }
                        } else {
                            let patient = await PatientModel.getByUserID(results[i].from_userid, '*', true);
                            if (patient.length > 0) {
                                patient = patient[0];
                                results[i].firstname = patient.firstname;
                                results[i].lastname = patient.lastname;
                                results[i].image_url = '';
                                if (patient.image_url) {
                                    results[i].image_url = config.originalServer + '' + patient.image_url;
                                }
                            }
                        }
                        if (i == (results.length - 1)) {
                            resolve(results);
                        }
                    }
                } else {
                    resolve(results);
                }
            });
        });
    });
};
obj.update = (messageID, data) => {
    return new Promise((resolve, reject) => {
        if (!messageID) {
            return reject({
                message: 'Please provide Message ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("UPDATE `messages` SET ? WHERE `id` = ?", [
                data,
                messageID
            ], (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve();
            });
        });
    });
};
obj.moveToTrash = (messageID, patientID) => {
    return new Promise(async (resolve, reject) => {
        if (!messageID) {
            return reject({
                message: 'Please provide Message ID.',
                bool: false
            });
        }
        if (!patientID) {
            return reject({
                message: 'Please provide Patient ID.',
                bool: false
            });
        }
        const data = {};
        try {
            let message = await obj.getByID(messageID);
            if (message.length > 0) {
                message = message[0];
                if (message.from_userid == patientID) {
                    data.sender_delete = 1;
                } else {
                    data.recipient_delete = 1;
                }
                pool.getConnection((err, connection) => {
                    if (err) {
                        // connection.release();
                        return reject({
                            message: 'There is an error while connecting to the database. Please try again.',
                            bool: false
                        });
                    }
                    connection.query("UPDATE `messages` SET ? WHERE `id` = ?", [
                        data,
                        messageID
                    ], (err, results) => {
                        connection.release();
                        if (err) {
                            return reject({
                                message: 'There is an error while querying to the database. Please try again.',
                                bool: false
                            });
                        }
                        resolve();
                    });
                });
            } else {
                reject({
                    message: 'Invalid message selected.',
                    bool: false
                });
            }
        } catch (e) {
            reject(e);
        }
    });
};
obj.permanentDelete = (messageID, patientID) => {
    return new Promise(async (resolve, reject) => {
        if (!messageID) {
            return reject({
                message: 'Please provide Message ID.',
                bool: false
            });
        }
        if (!patientID) {
            return reject({
                message: 'Please provide Patient ID.',
                bool: false
            });
        }
        const data = {};
        try {
            let message = await obj.getByID(messageID);
            if (message.length > 0) {
                message = message[0];
                if (message.from_userid == patientID) {
                    data.sender_delete = 2;
                } else {
                    data.recipient_delete = 2;
                }
                pool.getConnection((err, connection) => {
                    if (err) {
                        // connection.release();
                        return reject({
                            message: 'There is an error while connecting to the database. Please try again.',
                            bool: false
                        });
                    }
                    connection.query("UPDATE `messages` SET ? WHERE `id` = ?", [
                        data,
                        messageID
                    ], (err, results) => {
                        connection.release();
                        if (err) {
                            return reject({
                                message: 'There is an error while querying to the database. Please try again.',
                                bool: false
                            });
                        }
                        resolve();
                    });
                });
            } else {
                reject({
                    message: 'Invalid message selected.',
                    bool: false
                });
            }
        } catch (e) {
            reject(e);
        }
    });
};
obj.fixMessageForDisplay = (message, patientID) => {
    return new Promise(async (resolve, reject) => {
        if (!patientID) {
            return reject({
                message: 'Please provide PatientID ID.',
                bool: false
            });
        }
        try {
            let user = [];
            let type = 'user';
            if (patientID == message.from_userid) {
                //I'm the sender
                user = await PatientModel.getByUserID(message.from_userid, 'firstname, lastname, image_url', true);
                type = 'sender';
            } else {
                //I'm the receiver, so if I'm patient look into doctor table, otherwise doctor table
                user = await DoctorModel.getByUserID(message.from_userid, 'firstname, lastname, image_url', true);
                type = 'receiver';
            }
            if (user.length <= 0) {
                return reject({
                    message: 'No ' + type + ' found.',
                    bool: false
                });
            }
            user = user[0];
            message.sender_name = user.firstname + ' ' + user.lastname;
            message.sender_image_url = config.originalServer + '' + user.image_url;

            user = [];
            type = 'user';
            if (message.to_userid == patientID) {
                user = await PatientModel.getByUserID(message.to_userid, 'firstname, lastname, image_url', true);
                type = 'receiver';
            } else {
                user = await DoctorModel.getByUserID(message.to_userid, 'firstname, lastname, image_url', true);
                type = 'sender';
            }
            if (user.length <= 0) {
                return reject({
                    message: 'No ' + type + ' found.',
                    bool: false
                });
            }
            user = user[0];
            message.receiver_name = user.firstname + ' ' + user.lastname;
            message.receiver_image_url = config.originalServer + '' + user.image_url;
            resolve(message);
        } catch (e) {
            reject(e);
        }
    });
};
obj.getLatestFiveReplysForMessage = (messageID, patientID, replies) => {
    return new Promise(async (resolve, reject) => {
        if (!messageID) {
            return reject({
                message: 'Please provide Message ID.',
                bool: false
            });
        }
        if (!patientID) {
            return reject({
                message: 'Please provide Patient ID.',
                bool: false
            });
        }
        // const messageReplies = [];
        try {
            let message = await obj.getByID(messageID);
            if (message.length <= 0) {
                return reject({
                    message: 'Invalid message selected.',
                    bool: false
                });
            }
            message = message[0];
            if (message.from_userid == patientID) {
                if (message.sender_delete == 1 || message.sender_delete == 2) {
                    return resolve(replies);
                }
            } else {
                if (message.recipient_delete == 1 || message.recipient_delete == 2) {
                    return resolve(replies);
                }
            }
            message = await obj.fixMessageForDisplay(message, patientID);
            if (message.length <= 0) {
                return reject({
                    message: 'Invalid message selected.',
                    bool: false
                });
            }
            replies.push(message);
            if (message.parent > 0) {
                replies = await obj.getLatestFiveReplysForMessage(message.parent, patientID, replies);
            }
            resolve(replies);
        } catch (e) {
            reject(e);
        }
    });
};
module.exports = obj;