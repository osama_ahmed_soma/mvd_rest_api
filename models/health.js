const pool = require('../config/dbConnection');
const health = {};
health.getData = data => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `health` WHERE `patients_id` = ? AND `type` = ?", [data.patientID, data.type], (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                if (results.length > 0) {
                    return resolve(results);
                }
                reject({
                    message: 'There is no health data.',
                    bool: false
                });
            });
        });
    });
};
health.getByID = ID => {
    return new Promise((resolve, reject) => {
        if (!ID) {
            return reject({
                message: 'Please provide Health ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `health` WHERE `id` = ? LIMIT 1", [ID], (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                if (results.length > 0) {
                    return resolve(results[0]);
                }
                reject({
                    message: 'There is no health data.',
                    bool: false
                });
            });
        });
    });
};
const populateInsertData = object => {
    return new Promise(resolve => {
        const keys = Object.keys(object);
        let questions = [];
        let bindedData = [];
        for (let i = 0; i < keys.length; i++) {
            questions.push('?');
            bindedData.push(object[keys[i]]);
            if (i == (keys.length - 1)) {
                return resolve({
                    fields: keys.toString(),
                    questions: questions.toString(),
                    bindedData: bindedData
                });
            }
        }
    });
};
health.insert = data => {
    return new Promise(async (resolve, reject) => {
        const array = Object.keys(data);
        if (array.length <= 0) {
            return reject({
                message: 'No Data Provided.',
                bool: false
            });
        }
        const populateData = await populateInsertData(data);
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("INSERT INTO `health` (" + populateData.fields + ") VALUES (" + populateData.questions + ")", populateData.bindedData, (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve(results.insertId);
            });
        });
    });
};
health.update = (healthID, data) => {
    return new Promise((resolve, reject) => {
        if (!healthID) {
            return reject({
                message: 'Please provide Health ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("UPDATE `health` SET ? WHERE `id` = ?", [
                data,
                healthID
            ], async (err) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        error: err,
                        bool: false
                    });
                }
                resolve();
            });
        });
    });
};
health.remove = healthID => {
    return new Promise((resolve, reject) => {
        if (!healthID) {
            return reject({
                message: 'Please provide Health ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("DELETE FROM `health` WHERE `id` = ?", [
                healthID
            ], (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve();
            });
        });
    });
};

module.exports = health;