const fs = require('fs');
const path = require('path');
const pool = require('../config/dbConnection');

// Config
const config = require('../config/config');

// Models
const SharedFilesModel = require('./shared_files');

const files_upload = {};
files_upload.getByID = fileID => {
    return new Promise((resolve, reject) => {
        if (!fileID) {
            return reject({
                message: 'Please provide File ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `files_upload` WHERE `id` = ? LIMIT 1", [
                fileID
            ], (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve(results);
            });
        });
    });
};
files_upload.getAll = patientID => {
    return new Promise((resolve, reject) => {
        if (!patientID) {
            return reject({
                message: 'Please provide Patient ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `files_upload` WHERE `user_id` = ? ORDER BY `id` DESC", [
                patientID
            ], (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve(results);
            });
        });
    });
};
files_upload.insert = data => {
    return new Promise((resolve, reject) => {
        if (!data.patientID) {
            return reject({
                message: 'Please provide Patient ID.',
                bool: false
            });
        }
        if (!data.file) {
            return reject({
                message: 'File object is not attached.',
                bool: false
            });
        }
        if (!data.file.name) {
            return reject({
                message: 'Please provide File Name.',
                bool: false
            });
        }
        if (!data.file.size) {
            return reject({
                message: 'Please provide File Size.',
                bool: false
            });
        }
        if (!data.file.type) {
            return reject({
                message: 'Please provide File Type.',
                bool: false
            });
        }
        data.title = data.title || 'Untitled File';
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("INSERT INTO `files_upload` (`user_id`, `filename`, `filesize`, `filetype`, `title`) VALUES (?, ?, ?, ?, ?)", [
                data.patientID,
                data.file.name,
                data.file.size,
                data.file.type,
                data.title
            ], (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        error: err,
                        bool: false
                    });
                }
                resolve();
            });
        });
    });
};
files_upload.update = (fileID, body) => {
    return new Promise((resolve, reject) => {
        if (!fileID) {
            return reject({
                message: 'Please provide Document ID.',
                bool: false
            });
        }
        body.title = body.title || '';
        if (body.title == '') {
            body.title = 'Untitled File';
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("UPDATE `files_upload` SET ? WHERE `id` = ?", [
                body,
                fileID
            ], (err) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve();
            });
        });
    });
};
files_upload.remove = (fileID, patientID) => {
    return new Promise(async (resolve, reject) => {
        if (!fileID) {
            return reject({
                message: 'Please provide File ID.',
                bool: false
            });
        }
        try {
            let file = await files_upload.getByID(fileID);
            if (file.length <= 0) {
                return reject({
                    message: 'This document is not exist in records.',
                    bool: false
                });
            }
            file = file[0];
            pool.getConnection((err, connection) => {
                if (err) {
                    // connection.release();
                    return reject({
                        message: 'There is an error while connecting to the database. Please try again.',
                        bool: false
                    });
                }
                connection.query("DELETE FROM `files_upload` WHERE `id` = ?", [
                    fileID
                ], async (err) => {
                    connection.release();
                    if (err) {
                        return reject({
                            message: 'There is an error while querying to the database. Please try again.',
                            bool: false
                        });
                    }
                    try {
                        await SharedFilesModel.remove(fileID, patientID);
                        const filePath = config.basePath + path.sep + 'uploads' + path.sep + 'documents' + path.sep + 'patient' + path.sep + patientID + path.sep + file.filename;
                        if (fs.existsSync(filePath)) {
                            fs.unlinkSync(filePath);
                            resolve();
                        } else {
                            resolve();
                        }
                    } catch (e) {
                        reject(e);
                    }
                });
            });
        } catch (e) {
            reject(e);
        }
    });
};
module.exports = files_upload;