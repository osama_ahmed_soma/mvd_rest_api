const config = require('../config/config');

const curl = require('curlrequest');
const pool = require('../config/dbConnection');
const user = {};
user.isExists = (userID) => {
    return new Promise((resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide User ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `user` WHERE `id` = ? LIMIT 1", [userID], (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                if (results.length > 0) {
                    return resolve(true);
                }
                resolve(false);
            });
        });
    });
};
user.getByID = (userID) => {
    return new Promise((resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide User ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `user` WHERE `id` = ? LIMIT 1", [userID], (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                if (results.length > 0) {
                    return resolve(results[0]);
                }
                reject({
                    message: 'No User Found.',
                    bool: false
                });
            });
        });
    });
};
user.login = (params) => {
    return new Promise((resolve, reject) => {
        const options = {};
        options.url = config.server_path + 'login/login_apiCheck?email=' + params.email + '&password=' + params.password;
        curl.request(options, (err, userData) => {
            if (err) {
                reject({
                    message: 'Something went wrong',
                    err: err,
                    bool: false
                });
            } else {
                if (/^[\],:{}\s]*$/.test(userData.replace(/\\["\\\/bfnrtu]/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
                    resolve(JSON.parse(userData));
                } else {
                    reject({
                        message: 'Something went wrong',
                        bool: false
                    });
                }
            }
        });
    });
};
user.signup = (params) => {
    // check if this email is already exists or not
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `user` WHERE `user_email` = ? LIMIT 1", [params.email], (err, results, fields) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'Something went wrong. Please try again',
                        bool: false
                    });
                }
                if (results.length > 0) {
                    return reject({
                        message: 'This user is already exists.',
                        bool: false
                    });
                }
                const options = {};
                options.url = config.server_path + 'register/create_patient_api?userFname=' + params.first_name + '&userLname=' + params.last_name + '&userEmail=' + params.email;
                curl.request(options, (err, userData) => {
                    if (err) {
                        return reject({
                            message: 'Something went wrong. Please try again',
                            bool: false
                        });
                    }
                    resolve(JSON.parse(userData));
                });
            });
        });
    });
};
user.generateToken = email => {
    return new Promise((resolve, reject) => {
        const options = {};
        options.url = config.server_path + 'password/reset_api?email=' + email;
        curl.request(options, (err, res) => {
            if (err) {
                reject({
                    message: 'Something went wrong',
                    bool: false
                });
            } else {
                res = JSON.parse(res);
                resolve(res);
            }
        });
    });
};
user.changePassword = (token, email, password) => {
    return new Promise((resolve, reject) => {
        const options = {};
        options.url = `${config.server_path}password/change_api/${token}?email=${email}&password=${password}`;
        curl.request(options, (err, res) => {
            if (err) {
                reject({
                    message: 'Something went wrong',
                    bool: false
                });
            } else {
                res = JSON.parse(res);
                resolve(res);
            }
        });
    });
};
user.newPassword = (userID, currentPassword, password) => {
    return new Promise((resolve, reject) => {
        const options = {};
        options.url = `${config.server_path}password/new_api/${userID}/${currentPassword}/${password}`;
        console.log(options);
        curl.request(options, (err, res) => {
            if (err) {
                reject({
                    message: 'Something went wrong',
                    bool: false
                });
            } else {
                if (/^[\],:{}\s]*$/.test(res.replace(/\\["\\\/bfnrtu]/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
                    resolve(JSON.parse(res));
                } else {
                    reject({
                        message: 'Something went wrong',
                        bool: false
                    });
                }
            }
        });
    });
};
user.update = (id, data) => {
    return new Promise((resolve, reject) => {
        if (!id) {
            return reject({
                message: 'Please provide User ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("UPDATE `user` SET ? WHERE `id` = ?", [
                data,
                id
            ], async (err) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        error: err,
                        bool: false
                    });
                }
                resolve();
            });
        });
    });
};
user.searchDoctor = params => {
    return new Promise((resolve, reject) => {
        const text = params.query || '';
        const state = params.state || '';
        const specialty = params.specialty || '';

        const selectData = "`doctor.id`, `doctor.firstname`, `doctor.lastname`, `doctor.image_url`, `doctor.userid`, `doctor.location`";
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT " + selectData + " FROM `doctor` WHERE `to_userid` = ? AND `view` = 0 AND `deleted` = 0", [userID], (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: err,
                        bool: false
                    });
                }
                resolve(results.length);
            });
        });
    });
};
module.exports = user;