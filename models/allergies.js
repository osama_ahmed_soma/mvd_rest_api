const pool = require('../config/dbConnection');

const allergies = {};
allergies.getAll = () => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `allergies` ORDER BY `name` ASC", [], async (err, allergies) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        error: err,
                        bool: false
                    });
                }
                resolve(allergies);
            });
        });
    });
};

allergies.getByName = name => {
    return new Promise((resolve, reject) => {
        if(!name) {
            return reject({
                message: 'Please provide Allergy Name.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `allergies` WHERE `name` = ? LIMIT 1", [
                name
            ], async (err, allergy) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        error: err,
                        bool: false
                    });
                }
                if(allergy.length > 0) {
                    return resolve(allergy[0]);
                }
                resolve(false);
            });
        });
    });
};

allergies.insertIfNotExists = allergy => {
    return new Promise(async (resolve, reject) => {
        try {
            const allergyExists = await allergies.getByName(allergy);
            if(!allergyExists) {
                pool.getConnection((err, connection) => {
                    if (err) {
                        return reject({
                            message: 'There is an error while connecting to the database. Please try again.',
                            bool: false
                        });
                    }
                    connection.query("INSERT INTO `allergies` (`name`) VALUES (?)", [
                        allergy.trim()
                    ], async (err) => {
                        connection.release();
                        if (err) {
                            return reject({
                                message: 'There is an error while querying to the database. Please try again.',
                                error: err,
                                bool: false
                            });
                        }
                        resolve();
                    });
                });
            } else {
                resolve();
            }
        } catch (e) {
            reject(e);
        }
    });
};

module.exports = allergies;