const pool = require('../config/dbConnection');
const doctor_consultation_types = {};
doctor_consultation_types.getByID = id => {
    return new Promise((resolve, reject) => {
        if (!id) {
            return reject({
                message: 'Please provide Consultation Type ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `doctor_consultation_types` WHERE `id` = ? LIMIT 1", [
                id
            ], (err, result) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                if (result.length <= 0) {
                    return reject({
                        message: 'Invalid Consultation Type Selected.',
                        bool: false
                    });
                }
                resolve(result[0]);
            });
        });
    });
};
doctor_consultation_types.getByDoctorID = doctorID => {
    return new Promise((resolve, reject) => {
        if (!doctorID) {
            return reject({
                message: 'Please provide Doctor ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT `doctor_consultation_types`.`id`, `doctor_consultation_types`.`doctor_id`, `duration`.`time` as `duration`, `duration`.`id` as `duration_id`, `doctor_consultation_types`.`rate` FROM `doctor_consultation_types` JOIN `duration` ON `duration`.`id` = `doctor_consultation_types`.`duration_id` WHERE `doctor_consultation_types`.`doctor_id` = ? ORDER BY `doctor_consultation_types`.`id` ASC", [
                doctorID
            ], (err, result) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                console.log(result);
                resolve(result);
            });
        });
    });
};
module.exports = doctor_consultation_types;