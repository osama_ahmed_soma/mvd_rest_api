const pool = require('../config/dbConnection');

// Config
const config = require('../config/config');

// Models
const User = require('./user');
const Doctor = require('./doctor');
const DoctorSpecialization = require('./doctor_specialization');

const favorite = {};
favorite.getMyDoctors = userID => {
    return new Promise(async (resolve, reject) => {
        try {
            if (!userID) {
                return reject({
                    message: 'Please provide User ID.',
                    bool: false
                });
            }
            const isUserExists = await User.isExists(userID);
            if (!isUserExists) {
                return reject({
                    message: 'This user is not exists.',
                    bool: false
                });
            }
            pool.getConnection((err, connection) => {
                if (err) {
                    // connection.release();
                    return reject({
                        message: 'There is an error while connecting to the database. Please try again.',
                        bool: false
                    });
                }
                connection.query("SELECT `favorite`.* FROM `favorite`, `doctor` WHERE `favorite`.`p_id` = ? AND `doctor`.`userid` = `favorite`.`d_id`", [userID], async (err, favoriteData) => {
                    connection.release();
                    if (err) {
                        return reject({
                            message: 'There is an error while querying to the database. Please try again.',
                            bool: false
                        });
                    }
                    resolve(favoriteData);
                });
            });
        } catch (e) {
            reject(e);
        }
    });
};

favorite.getByDoctorID = (patientID, doctorID, isBoolean = false) => {
    return new Promise((resolve, reject) => {
        if (!patientID) {
            return reject({
                message: 'Please provide Patient ID.',
                bool: false
            });
        }
        if (!doctorID) {
            return reject({
                message: 'Please provide Doctor ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `favorite` WHERE `p_id` = ? AND `d_id` = ?", [
                patientID,
                doctorID
            ], async (err, favoriteData) => {
                connection.release();
                try {
                    if (err) {
                        return reject({
                            message: 'There is an error while querying to the database. Please try again.',
                            bool: false
                        });
                    }
                    if (isBoolean) {
                        if (favoriteData.length > 0) {
                            resolve(true);
                        } else {
                            resolve(false);
                        }
                        return;
                    }
                    const mydoctors = [];
                    if (favoriteData.length > 0) {
                        for (let i = 0; i < favoriteData.length; i++) {
                            const doctorData = await Doctor.getByUserID(favoriteData[i].d_id, '`userid`, `firstname`, `lastname`, `country`, `image_url`', true);
                            if (doctorData.length > 0) {
                                doctorData[0].specialization = await DoctorSpecialization.getByDoctorID(favoriteData[i].d_id);
                                mydoctors.push(doctorData[0]);
                            }
                        }
                    }
                    resolve(mydoctors);
                } catch (e) {
                    reject(e);
                }
            });
        });
    });
};
favorite.remove = (patientID, doctorID) => {
    return new Promise((resolve, reject) => {
        if (!patientID) {
            return reject({
                message: 'Please provide Patient ID.',
                bool: false
            });
        }
        if (!doctorID) {
            return reject({
                message: 'Please provide Doctor ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("DELETE FROM `favorite` WHERE `p_id` = ? AND `d_id` = ?", [
                patientID,
                doctorID
            ], (err, favoriteData) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve();
            });
        });
    });
};

favorite.add = (patientID, doctorID) => {
    return new Promise(async (resolve, reject) => {
        if (!patientID) {
            return reject({
                message: 'Please provide Patient ID.',
                bool: false
            });
        }
        if (!doctorID) {
            return reject({
                message: 'Please provide Doctor ID.',
                bool: false
            });
        }

        if (await favorite.getByDoctorID(patientID, doctorID, true)) {
            return reject({
                message: 'Already Added to Favorite.',
                data: favorite.getByDoctorID(patientID, doctorID, true),
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("INSERT INTO `favorite` (`time`, `p_id`, `d_id`, `status`) VALUES (NOW(), ?, ?, 'pending')", [
                patientID,
                doctorID
            ], (err, favoriteData) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve();
            });
        });
    });
};


module.exports = favorite;