const moment = require('moment-timezone');
const nodemailer = require('nodemailer');
const pool = require('../config/dbConnection');
const config = require('../config/config');

// Stripe
const stripe = require('stripe');

// Models
const DoctorModel = require('./doctor');
const PatientModel = require('./patient');
const AvailableTimeModel = require('./available_time');
const ReasonModel = require('./reason');
const WaiveFeeModel = require('./waive_fee');
const DoctorConsultationTypesModel = require('./doctor_consultation_types');
const NotificationModel = require('./notification');

// Helper
const MainHelper = require('../helpers/main.helper');

const appointment = {};
appointment.create = data => {
    return new Promise((resolve, reject) => {
        //    INSERT INTO `appointment` (`patients_id`, `doctors_id`, `available_id`, `start_date_stamp`, `end_date_stamp`, `reason_id`, `reason`, `notes`, `created_date`, `session_start`, `session_end`, `stripe_transaction_id`, `amount`, `status`, `added`) VALUES ('1', '39', '339', '1497875400', '1497877200', '1', '', '', '2017-06-19 08:23:03', '2017-06-19 08:30:00', '2017-06-19 09:00:00', 'waived', '39', 3, 1497874983)
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("INSERT INTO `appointment` (`patients_id`, `doctors_id`, `available_id`, `start_date_stamp`, `end_date_stamp`, `reason_id`, `reason`, `notes`, `created_date`, `session_start`, `session_end`, `stripe_transaction_id`, `amount`, `status`, `added`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [
                data.patients_id,
                data.doctors_id,
                data.available_id,
                data.start_date_stamp,
                data.end_date_stamp,
                data.reason_id,
                data.reason,
                data.notes,
                data.created_date,
                data.session_start,
                data.session_end,
                data.stripe_transaction_id,
                data.amount,
                data.status,
                data.added
            ], async (err, result) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve(result.insertId);
            });
        });
    });
};
appointment.schedule = data => {
    return new Promise(async (resolve, reject) => {
        const doctorID = data.doctorID;
        const patientID = data.patientID;
        const availableTime = data.availableTime;
        const reasonID = data.reasonID;
        const consultationTypeID = data.consultationTypeID;
        const stripeToken = data.token;
        if (!doctorID) {
            return reject({
                message: 'Please provide Doctor ID.',
                bool: false
            });
        }
        if (!patientID) {
            return reject({
                message: 'Please provide Patient ID.',
                bool: false
            });
        }
        if (!reasonID) {
            return reject({
                message: 'Please provide Reason ID.',
                bool: false
            });
        }
        try {
            const doctor = await DoctorModel.getByUserID(doctorID);
            const patient = await PatientModel.getByUserID(patientID);
            const reason = await ReasonModel.getByID(reasonID);

            const tempStart = moment.unix(availableTime.start_time_stamp).tz(process.env.TZ).format('hh:mm A');
            const tempEnd = moment.unix(availableTime.end_time_stamp).tz(process.env.TZ).format('hh:mm A');

            let isWaveFee = await WaiveFeeModel.isPatientWaived(doctorID, patientID);
            let paymentRate;
            if (isWaveFee == 1) {
                paymentRate = doctor.fee;
            } else {
                if (!consultationTypeID) {
                    return reject({
                        message: 'Please provide Consultation Type ID.',
                        bool: false
                    });
                }
                const consultationTypeData = await DoctorConsultationTypesModel.getByID(consultationTypeID);
                paymentRate = consultationTypeData.rate;
            }
            const isAvailable = await appointment.checkAppointmentAvailable(doctorID, availableTime.start_date_stamp, availableTime.end_date_stamp);
            if (isAvailable > 0) {
                return reject({
                    message: `Selected time (${tempStart} - ${tempEnd}) is not available for new schedule.`,
                    bool: false
                });
            }
            let dataForDB = {};
            if (stripeToken !== '') {
                const stripeInstance = stripe(doctor.stripe_one);
                const charge = await stripeInstance.charges.create({
                    amount: (paymentRate * 100),
                    currency: 'usd',
                    source: stripeToken,
                    description: `My Virtual Doctor charge patient: ${patient.firstname} ${patient.lastname}, start date: ${tempStart}, end date: ${tempEnd}`,
                    metadata: {
                        ip_address: data.ipAddress,
                        full_name: patient.firstname + ' ' + patient.lastname,
                        address_line_1: patient.address,
                        address_line_2: '',
                        phone: patient.phone_home,
                        country: patient.country,
                        city: patient.city,
                        zip: patient.zip,
                        state: '',
                        patient_id: patientID,
                        start_date: tempStart,
                        end_date: tempEnd,
                        available_id: availableTime.availableID
                    }
                });
                const retrieve = await stripeInstance.charges.retrieve(charge.id);
                if (retrieve.status == 'succeeded') {
                    dataForDB.patients_id = patientID;
                    dataForDB.doctors_id = doctorID;
                    dataForDB.available_id = availableTime.availableID;
                    dataForDB.start_date_stamp = availableTime.start_time_stamp;
                    dataForDB.end_date_stamp = availableTime.end_time_stamp;
                    dataForDB.reason_id = reasonID;
                    dataForDB.reason = '';
                    dataForDB.notes = data.notes;
                    dataForDB.created_date = moment.tz((Math.round((new Date()).getTime() / 1000)), process.env.TZ).format('YYYY-MM-DD HH:mm:ss');
                    dataForDB.session_start = moment.unix(availableTime.start_time_stamp).tz(process.env.TZ).format('YYYY-MM-DD HH:mm:ss');
                    dataForDB.session_end = moment.unix(availableTime.end_time_stamp).tz(process.env.TZ).format('YYYY-MM-DD HH:mm:ss');
                    dataForDB.stripe_transaction_id = charge.id;
                    dataForDB.amount = paymentRate;
                    dataForDB.status = 3;
                    dataForDB.added = moment.tz((new Date()).getTime(), process.env.TZ).unix();
                } else {
                    return reject({
                        message: 'Payment is not completed. Your payment status is ' + retrieve.status,
                        bool: false
                    });
                }
            } else {
                if (isWaveFee == 1) {
                    // doctor enabled waive fee
                    dataForDB.patients_id = patientID;
                    dataForDB.doctors_id = doctorID;
                    dataForDB.available_id = availableTime.availableID;
                    dataForDB.start_date_stamp = availableTime.start_time_stamp;
                    dataForDB.end_date_stamp = availableTime.end_time_stamp;
                    dataForDB.reason_id = reasonID;
                    dataForDB.reason = '';
                    dataForDB.notes = data.notes;
                    dataForDB.created_date = moment.tz((Math.round((new Date()).getTime() / 1000)), process.env.TZ).format('YYYY-MM-DD HH:mm:ss');
                    dataForDB.session_start = moment.unix(availableTime.start_time_stamp).tz(process.env.TZ).format('YYYY-MM-DD HH:mm:ss');
                    dataForDB.session_end = moment.unix(availableTime.end_time_stamp).tz(process.env.TZ).format('YYYY-MM-DD HH:mm:ss');
                    dataForDB.stripe_transaction_id = 'waived';
                    dataForDB.amount = doctorID;
                    dataForDB.status = 3;
                    dataForDB.added = moment.tz((new Date()).getTime(), process.env.TZ).unix();
                } else {
                    // doctor disabled waive fee
                    return reject({
                        message: 'Invalid Input. Doctor didn\'t added free payment for this patient.',
                        bool: false
                    });
                }
            }
            if (Object.keys(dataForDB).length === 0 && dataForDB.constructor === Object) {
                return reject({
                    message: 'There is an error please try again.',
                    bool: false
                });
            } else {
                if (consultationTypeID != '') {
                    dataForDB.consultation_type = consultationTypeID;
                }
                const appointmentID = await appointment.create(dataForDB);
                const consultationURL = config.originalServer + 'doctor/consultations';
                await NotificationModel.add({
                    from_userid: patientID,
                    to_userid: doctorID,
                    from_usertype: 'patient',
                    to_usertype: 'doctor',
                    message: `<a href="${config.originalServer}doctor/patientinfo/${patientID}">${patient.firstname} ${patient.lastname}</a> requested a session on <a href="${consultationURL}">${tempStart}</a>`,
                    type: 'appointment',
                    added: moment.tz((new Date()).getTime(), process.env.TZ).unix()
                });

                let transporter = config.email.getTransporter(nodemailer);
                let mailOptions = {
                    from: '"Myvirtualdoctor.com" <info@myvirtualdoctor.com>', // sender address
                    to: doctor.user_email, // list of receivers
                    subject: 'MyVirtualDoctor – Consultation Notification', // Subject line
                    text: 'Hello world ?', // plain text body
                    html: `<html>
<head>
    <title></title>
</head>
<body>
    <h4>Dear ${doctor.firstname}</h4>
    <p></p>
    <p>Patient ${patient.firstname} ${patient.lastname} has scheduled an online consultation with you on <a href="${consultationURL}">${tempStart}</a></p>
    <p></p>
    <p>Please login to your account on My Virtual Doctor using the link below for more details.</p>
    <p></p>
    <p>Click this link to login <a href="${config.originalServer}login">${config.originalServer}login</a></p>
    <p></p>
    <p>Please don't hesitate to contact us at  1-855-80-DOCTOR or at info@myvirtualdoctor.com for further assistance.</p>
    <p></p>
    <p>Sincerely,<br />
My Virtual Doctor</p>
</body>
</html>`
                };
                transporter.sendMail(mailOptions, (error, info) => {
                    if (error) {
                        return reject({
                            message: error,
                            bool: false
                        });
                    }
                    resolve();
                });
            }
        } catch (e) {
            if ('type' in e) {
                reject({
                    message: e.message,
                    bool: false
                });
            } else {
                reject(e);
            }
        }
    });
};
appointment.updateByID = (appointmentID, data) => {
    return new Promise((resolve, reject) => {
        if (!appointmentID) {
            return reject({
                message: 'Please provide Appointment ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("UPDATE `appointment` SET ? WHERE `id` = ?", [
                data,
                appointmentID
            ], async (err, appointments) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        request: "UPDATE `appointment` SET " + MainHelper.replaceAll(JSON.stringify(data), {
                            '{': '',
                            '}': '',
                            '"': '',
                            ':': ' = '
                        }) + " WHERE `id` = ?",
                        error: err,
                        bool: false
                    });
                }
                resolve(appointments);
            });
        });
    });
};
appointment.getAll = userID => {
    return new Promise((resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide User ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT appointment.*, appointment.id AS appointment_id, appointment.status as app_status, IF(reason.id = 13, appointment.reason, reason.name) AS primary_symptom, doctor.firstname, doctor.lastname FROM `appointment` JOIN `doctor` ON `doctor`.`userid` = `appointment`.`doctors_id` JOIN `reason` ON `reason`.`id` = `appointment`.`reason_id` WHERE `patients_id` = ? AND `appointment`.`status` != 1 ORDER BY `appointment`.`start_date_stamp` DESC", [userID], async (err, appointments) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve(appointments);
            });
        });
    });
};
appointment.getByID = appointmentID => {
    return new Promise((resolve, reject) => {
        if (!appointmentID) {
            return reject({
                message: 'Please provide Appointment ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT appointment.*, appointment.id AS appointment_id, appointment.status as app_status, IF(reason.id = 13, appointment.reason, reason.name) AS primary_symptom, doctor.firstname, doctor.lastname FROM `appointment` JOIN `doctor` ON `doctor`.`userid` = `appointment`.`doctors_id` JOIN `reason` ON `reason`.`id` = `appointment`.`reason_id` WHERE `appointment`.`id` = ? ORDER BY `appointment`.`start_date_stamp` DESC", [
                appointmentID
            ], async (err, appointments) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                if (appointments.length > 0) {
                    resolve(appointments[0]);
                } else {
                    reject({
                        message: 'No Appointment with this ID ' + appointmentID,
                        bool: false
                    });
                }
            });
        });
    });
};
appointment.getByIDAndType = (appointmentID, type) => {
    return new Promise((resolve, reject) => {
        if (!appointmentID) {
            return reject({
                message: 'Please provide Appointment ID.',
                bool: false
            });
        }
        if (!type) {
            return reject({
                message: 'Appointment type is not defined.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT appointment.*, appointment.id AS appointment_id, appointment.status as app_status, IF(reason.id = 13, appointment.reason, reason.name) AS primary_symptom, doctor.firstname, doctor.lastname FROM `appointment` JOIN `doctor` ON `doctor`.`userid` = `appointment`.`doctors_id` JOIN `reason` ON `reason`.`id` = `appointment`.`reason_id` WHERE `appointment`.`id` = ? AND `appointment`.`type` = ? ORDER BY `appointment`.`start_date_stamp` DESC", [
                appointmentID,
                type
            ], async (err, appointments) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                if (appointments.length > 0) {
                    resolve(appointments[0]);
                } else {
                    resolve(false);
                }
            });
        });
    });
};
appointment.getPast = (userID, page = 1) => {
    return new Promise((resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide User ID.',
                bool: false
            });
        }
        if (!page || page < 1 || page === undefined) {
            page = 1;
        }
        const per_page = config.limit_on_each_query;
        const start_point = (page * per_page) - per_page;
        const currentTime = Math.floor(new Date().getTime() / 1000);
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT appointment.*, appointment.id AS appointment_id, appointment.status as app_status, IF(reason.id = 13, appointment.reason, reason.name) AS primary_symptom, doctor.firstname, doctor.lastname FROM `appointment` JOIN `doctor` ON `doctor`.`userid` = `appointment`.`doctors_id` JOIN `reason` ON `reason`.`id` = `appointment`.`reason_id` WHERE `patients_id` = ? AND ( `appointment`.`start_date_stamp` < ? OR `appointment`.`status` = 6 ) AND `appointment`.`status` != 5 ORDER BY `appointment`.`start_date_stamp` DESC LIMIT ?, ?", [
                userID,
                currentTime,
                start_point,
                per_page
            ], async (err, appointments) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                if (appointments.length <= 0) {
                    return resolve(appointments);
                }
                for (let i = 0; i < appointments.length; i++) {
                    appointments[i].reasonObject = await ReasonModel.getByID(appointments[i].reason_id);
                    appointments[i].start = {
                        date: {
                            week_day: moment.tz((appointments[i].start_date_stamp * 1000), process.env.TZ).format('dddd'),
                            day: moment.tz((appointments[i].start_date_stamp * 1000), process.env.TZ).format('Do'),
                            month: moment.tz((appointments[i].start_date_stamp * 1000), process.env.TZ).format('MMM'),
                            year: moment.tz((appointments[i].start_date_stamp * 1000), process.env.TZ).format('YYYY')
                        },
                        time: moment.tz((appointments[i].start_date_stamp * 1000), process.env.TZ).format('h:mm A')
                    };
                    appointments[i].end = {
                        time: moment.tz((appointments[i].end_date_stamp * 1000), process.env.TZ).format('h:mm A')
                    };
                    if (i == (appointments.length - 1)) {
                        resolve(appointments);
                    }
                }
            });
        });
    });
};
appointment.getUpcoming = (userID, page = 1) => {
    return new Promise((resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide User ID.',
                bool: false
            });
        }
        if (!page || page < 1 || page === undefined) {
            page = 1;
        }
        const per_page = config.limit_on_each_query;
        const start_point = (page * per_page) - per_page;
        const currentTime = Math.floor(new Date().getTime() / 1000);
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT appointment.*, appointment.id AS appointment_id, appointment.status as app_status, IF(appointment.reason_id = 13, appointment.reason, reason.name) AS primary_symptom, doctor.firstname, doctor.lastname FROM `appointment` JOIN `doctor` ON `doctor`.`userid` = `appointment`.`doctors_id` JOIN `reason` ON `reason`.`id` = `appointment`.`reason_id` WHERE `patients_id` = ? AND `appointment`.`start_date_stamp` >= ? AND ( `appointment`.`status` = 2 OR `appointment`.`status` = 3 ) ORDER BY `appointment`.`start_date_stamp` ASC LIMIT ?, ?", [
                userID,
                currentTime,
                start_point,
                per_page
            ], async (err, appointments) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                if (appointments.length <= 0) {
                    return resolve(appointments);
                }
                for (let i = 0; i < appointments.length; i++) {
                    const currentTimeStamp = moment().tz(process.env.TZ).unix();
                    const diffTime = appointments[i].start_date_stamp - currentTimeStamp;
                    appointments[i].start = {
                        date: {
                            week_day: moment.tz((appointments[i].start_date_stamp * 1000), process.env.TZ).format('dddd'),
                            day: moment.tz((appointments[i].start_date_stamp * 1000), process.env.TZ).format('Do'),
                            month: moment.tz((appointments[i].start_date_stamp * 1000), process.env.TZ).format('MMM'),
                            year: moment.tz((appointments[i].start_date_stamp * 1000), process.env.TZ).format('YYYY')
                        },
                        time: moment.tz((appointments[i].start_date_stamp * 1000), process.env.TZ).format('h:mm A'),
                        secondLeft: diffTime
                    };
                    appointments[i].end = {
                        time: moment.tz((appointments[i].end_date_stamp * 1000), process.env.TZ).format('h:mm A')
                    };
                    if (i == (appointments.length - 1)) {
                        resolve(appointments);
                    }
                }
            });
        });
    });
};
appointment.getPending = userID => {
    return new Promise((resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide User ID.',
                bool: false
            });
        }
        const currentTime = Math.floor(new Date().getTime() / 1000);
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT appointment.*, appointment.id AS appointment_id, appointment.status as app_status, IF(appointment.reason_id = 13, appointment.reason, reason.name) AS primary_symptom, doctor.firstname, doctor.lastname FROM `appointment` JOIN `doctor` ON `doctor`.`userid` = `appointment`.`doctors_id` JOIN `reason` ON `reason`.`id` = `appointment`.`reason_id` WHERE `patients_id` = ? AND `appointment`.`start_date_stamp` >= ? AND `appointment`.`status` = 1 ORDER BY `appointment`.`start_date_stamp` ASC", [userID, currentTime], async (err, appointments) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                if (appointments.length > 0) {
                    return resolve(appointments);
                }
                reject({
                    message: 'No Pending Appointment',
                    bool: false
                });
            });
        });
    });
};
appointment.getCancelled = (userID, page = 1) => {
    return new Promise((resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide User ID.',
                bool: false
            });
        }
        if (!page || page < 1 || page === undefined) {
            page = 1;
        }
        const per_page = config.limit_on_each_query;
        const start_point = (page * per_page) - per_page;
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT appointment.*, appointment.id AS appointment_id, appointment.status as app_status, IF(appointment.reason_id = 13, appointment.reason, reason.name) AS primary_symptom, doctor.firstname, doctor.lastname FROM `appointment` JOIN `doctor` ON `doctor`.`userid` = `appointment`.`doctors_id` JOIN `reason` ON `reason`.`id` = `appointment`.`reason_id` WHERE `patients_id` = ? AND `appointment`.`status` = 5 ORDER BY `appointment`.`start_date_stamp` ASC LIMIT ?, ?", [
                userID,
                start_point,
                per_page
            ], async (err, appointments) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                if (appointments.length <= 0) {
                    return resolve(appointments);
                }
                for (let i = 0; i < appointments.length; i++) {
                    appointments[i].start = {
                        date: {
                            week_day: moment.tz((appointments[i].start_date_stamp * 1000), process.env.TZ).format('dddd'),
                            day: moment.tz((appointments[i].start_date_stamp * 1000), process.env.TZ).format('Do'),
                            month: moment.tz((appointments[i].start_date_stamp * 1000), process.env.TZ).format('MMM'),
                            year: moment.tz((appointments[i].start_date_stamp * 1000), process.env.TZ).format('YYYY')
                        },
                        time: moment.tz((appointments[i].start_date_stamp * 1000), process.env.TZ).format('h:mm A')
                    };
                    appointments[i].end = {
                        time: moment.tz((appointments[i].end_date_stamp * 1000), process.env.TZ).format('h:mm A')
                    };
                    if (i == (appointments.length - 1)) {
                        resolve(appointments);
                    }
                }
            });
        });
    });
};


appointment.getByTimeAndPatientIDUpcoming = (patientID, currentTime, upcomingTime) => {
    return new Promise((resolve, reject) => {
        if (!patientID) {
            return reject({
                message: 'Please provide Patient ID.',
                bool: false
            });
        }
        //    SELECT `appointment`.*, `appointment`.`id` AS `appointment_id`, `reason`.`name` AS `primary_symptom`, `doctor`.`firstname`, `doctor`.`lastname`, `appointment`.`status` as `app_status` FROM `appointment` JOIN `doctor` ON `doctor`.`userid` = `appointment`.`doctors_id` JOIN `reason` ON `reason`.`id` = `appointment`.`reason_id` WHERE `patients_id` = '1' AND FROM_UNIXTIME(appointment.start_date_stamp, '%Y-%m-%d %H:%i:%s') <= '2017-08-02 07:08:00' AND FROM_UNIXTIME(appointment.start_date_stamp, '%Y-%m-%d %H:%i:%s') >= '2017-08-02 07:03:00' AND ( (`appointment`.`status` = 2 OR `appointment`.`status` = 3) ) ORDER BY `appointment`.`start_date_stamp` DESC LIMIT 1
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT `appointment`.*, `appointment`.`id` AS `appointment_id`, `reason`.`name` AS `primary_symptom`, `doctor`.`firstname`, `doctor`.`lastname`, `appointment`.`status` as `app_status` FROM `appointment` JOIN `doctor` ON `doctor`.`userid` = `appointment`.`doctors_id` JOIN `reason` ON `reason`.`id` = `appointment`.`reason_id` WHERE `patients_id` = ? AND `appointment`.`start_date_stamp` <= ? AND `appointment`.`start_date_stamp` >= ? AND ( (`appointment`.`status` = 2 OR `appointment`.`status` = 3) ) ORDER BY `appointment`.`start_date_stamp` DESC LIMIT 1", [
                patientID,
                upcomingTime,
                currentTime
            ], (err, appointment) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve(appointment);
            });
        });
    });
};
appointment.getByTimeAndPatientIDCurrent = (patientID, currentTime) => {
    return new Promise((resolve, reject) => {
        if (!patientID) {
            return reject({
                message: 'Please provide Patient ID.',
                bool: false
            });
        }
        //    SELECT `appointment`.*, `appointment`.`id` AS `appointment_id`, `reason`.`name` AS `primary_symptom`, `doctor`.`firstname`, `doctor`.`lastname`, `appointment`.`status` as `app_status` FROM `appointment` JOIN `doctor` ON `doctor`.`userid` = `appointment`.`doctors_id` JOIN `reason` ON `reason`.`id` = `appointment`.`reason_id` WHERE `patients_id` = '1' AND FROM_UNIXTIME(appointment.start_date_stamp, '%Y-%m-%d %H:%i:%s') <= '2017-08-02 07:08:00' AND FROM_UNIXTIME(appointment.start_date_stamp, '%Y-%m-%d %H:%i:%s') >= '2017-08-02 07:03:00' AND ( (`appointment`.`status` = 2 OR `appointment`.`status` = 3) ) ORDER BY `appointment`.`start_date_stamp` DESC LIMIT 1
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT `appointment`.*, `appointment`.`id` AS `appointment_id`, `reason`.`name` AS `primary_symptom`, `doctor`.`firstname`, `doctor`.`lastname`, `appointment`.`status` as `app_status` FROM `appointment` JOIN `doctor` ON `doctor`.`userid` = `appointment`.`doctors_id` JOIN `reason` ON `reason`.`id` = `appointment`.`reason_id` WHERE `patients_id` = ? AND `appointment`.`start_date_stamp` <= ? AND `appointment`.`end_date_stamp` >= ? AND ( (`appointment`.`status` = 2 OR `appointment`.`status` = 3) ) ORDER BY `appointment`.`start_date_stamp` DESC LIMIT 1", [
                patientID,
                currentTime,
                currentTime
            ], (err, appointment) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve(appointment);
            });
        });
    });
};
appointment.checkAppointmentAvailable = (userID, startDate, endDate) => {
    return new Promise((resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide User ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT COUNT(*) AS `numrows` FROM `appointment` WHERE `doctors_id` = ? AND `start_date_stamp` >= ? AND `end_date_stamp` <= ?", [
                userID,
                startDate,
                endDate
            ], (err, appointmentsCount) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                return resolve(appointmentsCount[0].numrows);
            });
        });
    });
};
appointment.isDoctorReady = (appointmentID, userID) => {
    // SELECT * FROM `appointment` WHERE `id` = '212' AND `patients_id` = '1' AND `is_doctor_ready` = 1
    return new Promise((resolve, reject) => {
        if (!appointmentID) {
            return reject({
                message: 'Please provide Appointment ID.',
                bool: false
            });
        }
        if (!userID) {
            return reject({
                message: 'Please provide User ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `appointment` WHERE `id` = ? AND `patients_id` = ? AND `is_doctor_ready` = ?", [
                appointmentID,
                userID,
                1
            ], (err, appointment) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                if (appointment.length > 0) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            });
        });
    });
};
//SELECT * FROM `appointment` WHERE `id` = 1 AND `patients_id` = 1 AND `is_doctor_ready` = 1
appointment.isDoctorEndConsultation = (appointmentID, userID) => {
    return new Promise((resolve, reject) => {
        if (!appointmentID) {
            return reject({
                message: 'Please provide Appointment ID.',
                bool: false
            });
        }
        if (!userID) {
            return reject({
                message: 'Please provide User ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `appointment` WHERE `id` = ? AND `patients_id` = ? AND `is_doctor_ready` = 2", [
                appointmentID,
                userID
            ], (err, appointment) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                if (appointment.length > 0) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            });
        });
    });
};
module.exports = appointment;