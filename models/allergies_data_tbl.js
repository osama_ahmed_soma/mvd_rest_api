const pool = require('../config/dbConnection')
const allergies_data_tbl = {}

allergies_data_tbl.getAll = () => {
  return new Promise((resolve, reject) => {
    pool.getConnection((err, connection) => {
      if (err) {
        return reject({
          message: 'There is an error while connecting to the database. Please try again.',
          bool: false
        })
      }
      connection.query('SELECT * FROM `allergies_data_tbl`', [], (err, results) => {
        connection.release()
        if (err) {
          return reject({
            message: 'There is an error while querying to the database. Please try again.',
            bool: false
          })
        }
        resolve(results)
      })
    })
  })
}

module.exports = allergies_data_tbl