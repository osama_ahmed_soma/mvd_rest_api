const pool = require('../config/dbConnection');

const reason = {};

reason.getAll = () => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `reason` ORDER BY `name` ASC", [], (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve(results);
            });
        });
    });
};

reason.getByID = id => {
    return new Promise((resolve, reject) => {
        if (!id) {
            return reject({
                message: 'Please provide Reason ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `reason` WHERE `id` = ?", [id], (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                if (results.length > 0) {
                    return resolve(results[0]);
                }
                reject({
                    message: 'Reason is not exists.',
                    bool: false
                });
            });
        });
    });
};

module.exports = reason;
