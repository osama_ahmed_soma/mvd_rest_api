const pool = require('../config/dbConnection');
const waive_fee = {};

waive_fee.getByDoctorAndPatientID = (doctorID, patientID) => {
    return new Promise((resolve, reject) => {
        if (!doctorID) {
            return reject({
                message: 'Please provide Doctor ID.',
                bool: false
            });
        }
        if (!patientID) {
            return reject({
                message: 'Please provide Patient ID.',
                bool: false
            });
        }
    });
};

waive_fee.isPatientWaived = (doctorID, patientID) => {
    return new Promise((resolve, reject) => {
        if (!doctorID) {
            return reject({
                message: 'Please provide Doctor ID.',
                bool: false
            });
        }
        if (!patientID) {
            return reject({
                message: 'Please provide Patient ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT COUNT(*) AS `numrows` FROM `waive_fee` WHERE `doctor_id` = ? AND `patient_id` = ? LIMIT 1", [
                doctorID,
                patientID
            ], (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve(results[0].numrows);
            });
        });
    });
};

module.exports = waive_fee;

