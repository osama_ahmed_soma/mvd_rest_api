const pool = require('../config/dbConnection');
const LanguageModel = require('./language');
const doctor_language = {};
doctor_language.getByUserID = userID => {
    return new Promise((resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide Doctor ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `doctor_language` WHERE `userid` = ?", [userID], async (err, languageIDs) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                try {
                    const languages = [];
                    if (languageIDs.length > 0) {
                        for (let i = 0; i < languageIDs.length; i++) {
                            const language = await LanguageModel.getByID(languageIDs[i].language_id);
                            if (language.length > 0) {
                                languages.push(language[0].name);
                            }
                        }
                    }
                    resolve(languages);
                } catch (e) {
                    reject(e);
                }
            });
        });
    });
};
module.exports = doctor_language;