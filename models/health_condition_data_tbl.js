const pool = require('../config/dbConnection')
const health_condition_data_tbl = {}

health_condition_data_tbl.getAll = () => {
  return new Promise((resolve, reject) => {
    pool.getConnection((err, connection) => {
      if (err) {
        return reject({
          message: 'There is an error while connecting to the database. Please try again.',
          bool: false
        })
      }
      connection.query('SELECT * FROM `health_condition_data_tbl`', [], (err, results) => {
        connection.release()
        if (err) {
          return reject({
            message: 'There is an error while querying to the database. Please try again.',
            bool: false
          })
        }
        resolve(results)
      })
    })
  })
}

module.exports = health_condition_data_tbl