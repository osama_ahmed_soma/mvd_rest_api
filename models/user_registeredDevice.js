const pool = require('../config/dbConnection');
const user_registeredDevice = {};

user_registeredDevice.getAll = () => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `user_registeredDevice`", [], (err, result) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve(result);
            });
        });
    });
};

user_registeredDevice.getByUserID = userID => {
    return new Promise((resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide Device ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `user_registeredDevice` WHERE `userID` = ?", [
                userID
            ], (err, result) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve(result);
            });
        });
    });
};

user_registeredDevice.getByDeviceID = deviceID => {
    return new Promise(async (resolve, reject) => {
        try {
            const data = await user_registeredDevice.isDeviceExists(deviceID);
            if(!data) {
                return reject({
                    message: 'Device is not registered.',
                    bool: false
                });
            }
            resolve(data);
        } catch (e) {
            reject(e);
        }
    });
};

user_registeredDevice.isDeviceExists = (deviceID) => {
    return new Promise((resolve, reject) => {
        if (!deviceID) {
            return reject({
                message: 'Please provide Device ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `user_registeredDevice` WHERE `deviceID` = ?", [
                deviceID
            ], (err, result) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                if (result.length > 0) {
                    resolve(result[0]);
                } else {
                    resolve(false);
                }
            });
        });
    });
};

user_registeredDevice.insert = (deviceID, userID, deviceType) => {
    return new Promise((resolve, reject) => {
        if (!deviceID) {
            return reject({
                message: 'Please provide Device ID.',
                bool: false
            });
        }
        if (!userID) {
            return reject({
                message: 'Please provide User ID.',
                bool: false
            });
        }
        if (deviceType !== 'android' && deviceType !== 'ios') {
            return reject({
                message: 'Please provide Device Type (Android or IOS) only.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("INSERT INTO `user_registeredDevice` (`deviceID`, `deviceType`, `userID`) VALUES (?, ?, ?)", [
                deviceID,
                deviceType,
                userID
            ], (err) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve();
            });
        });
    });
};

user_registeredDevice.updateByDeviceID = (data, deviceID) => {
    return new Promise((resolve, reject) => {
        if (!deviceID) {
            return reject({
                message: 'Please provide Device ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("UPDATE `user_registeredDevice` SET ? WHERE `deviceID` = ?", [
                data,
                deviceID
            ], (err) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve();
            });
        });
    });
};

user_registeredDevice.removeByDeviceID = deviceID => {
    return new Promise((resolve, reject) => {
        if (!deviceID) {
            return reject({
                message: 'Please provide Device ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("DELETE FROM `user_registeredDevice` WHERE `deviceID` = ?", [
                deviceID
            ], (err) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve();
            });
        });
    });
};

user_registeredDevice.removeByDeviceIDAndUserID = (deviceID, userID) => {
    return new Promise((resolve, reject) => {
        if (!deviceID) {
            return reject({
                message: 'Please provide Device ID.',
                bool: false
            });
        }
        if (!userID) {
            return reject({
                message: 'Please provide User ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("DELETE FROM `user_registeredDevice` WHERE `deviceID` = ? AND `userID` = ?", [
                deviceID,
                userID
            ], (err) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve();
            });
        });
    });
};

module.exports = user_registeredDevice;