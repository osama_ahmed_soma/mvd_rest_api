const pool = require('../config/dbConnection');

// Models
const AppointmentModel = require('./appointment');

const pushNotification_temp = {};

pushNotification_temp.isExists = (rowID, type = 'notification') => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `pushNotification_temp` WHERE `rowID` = ? AND `type` = ? LIMIT 1", [
                rowID,
                type
            ], (err, result) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                if (result.length > 0) {
                    resolve(result[0]);
                } else {
                    resolve(false);
                }
            });
        });
    });
};

pushNotification_temp.isExistsByRowID = (rowID) => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `pushNotification_temp` WHERE `rowID` = ? LIMIT 1", [
                rowID
            ], (err, result) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                if (result.length > 0) {
                    resolve(result[0]);
                } else {
                    resolve(false);
                }
            });
        });
    });
};

pushNotification_temp.isExistsBySubIDAndType = (subID, type) => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `pushNotification_temp` WHERE `subID` = ?", [
                subID
            ], async (err, result) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                if (result.length > 0) {
                    let isExists = false;
                    for (let i = 0; i < result.length; i++) {
                        const appointmentData = await AppointmentModel.getByIDAndType(subID, type);
                        if(appointmentData) {
                            isExists = true;
                        }
                        if(i === (result.length - 1)) {
                            resolve(isExists);
                        }
                    }

                } else {
                    resolve(false);
                }
            });
        });
    });
};

pushNotification_temp.insert = (rowID, type) => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("INSERT INTO `pushNotification_temp` (`rowID`, `type`) VALUES (?, ?)", [
                rowID,
                type
            ], (err) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve();
            });
        });
    });
};

pushNotification_temp.update = (data, id) => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("UPDATE `pushNotification_temp` SET ? WHERE `id` = ?", [
                data,
                id
            ], (err) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve();
            });
        });
    });
};

module.exports = pushNotification_temp;