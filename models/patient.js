const pool = require('../config/dbConnection');
const User = require('./user');
const patient = {};
// patient.getByUserID = userID => {
patient.getByUserID = (userID, customFields, isErrorHide) => {
    return new Promise(async (resolve, reject) => {
        try {
            if (!userID) {
                return reject({
                    message: 'Please provide User ID.',
                    bool: false
                });
            }
            const isUserExists = await User.isExists(userID);
            if (!isUserExists) {
                return reject({
                    message: 'This user is not exists.',
                    bool: false
                });
            }
            customFields = customFields || '*';
            isErrorHide = isErrorHide || false;
            pool.getConnection((err, connection) => {
                if (err) {
                    // connection.release();
                    return reject({
                        message: 'There is an error while connecting to the database. Please try again.',
                        bool: false
                    });
                }
                connection.query("SELECT " + customFields + " FROM `patient` WHERE `user_id` = ? LIMIT 1", [userID], (err, patient) => {
                    connection.release();
                    if (err) {
                        return reject({
                            message: 'There is an error while querying to the database. Please try again.',
                            bool: false
                        });
                    }
                    if (isErrorHide) {
                        resolve(patient);
                    } else {
                        if (patient.length > 0) {
                            return resolve(patient[0]);
                        }
                        reject({
                            message: 'Patient is not exists.',
                            // message: isErrorHide,
                            bool: false
                        });
                    }
                });
            });
        } catch (e) {
            reject(e);
        }
    });
};

patient.updateByUserID = (patientID, data) => {
    return new Promise((resolve, reject) => {
        if (!patientID) {
            return reject({
                message: 'Please provide Patient ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("UPDATE `patient` SET ? WHERE `user_id` = ?", [
                data,
                patientID
            ], async (err, patient) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        error: err,
                        bool: false
                    });
                }
                resolve(patient);
            });
        });
    });
};

module.exports = patient;