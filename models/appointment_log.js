// DB Connection
const pool = require('../config/dbConnection');

const appointment_log = {};
appointment_log.getByAppointmentID = appointmentID => {
    return new Promise((resolve, reject) => {
        if (!appointmentID) {
            return reject({
                message: 'Please provide Appointment ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT `appointment_log`.* FROM `appointment_log` JOIN `appointment` ON `appointment`.`id` = `appointment_log`.`appointment_id` WHERE `appointment_id` = ? LIMIT 1", [
                appointmentID
            ], (err, appointmentLogs) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve(appointmentLogs);
            });
        });
    });
};
appointment_log.getByAppointmentIDNotCancelled = appointmentID => {
    return new Promise((resolve, reject) => {
        if (!appointmentID) {
            return reject({
                message: 'Please provide Appointment ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT `appointment_log`.* FROM `appointment_log` JOIN `appointment` ON `appointment`.`id` = `appointment_log`.`appointment_id` WHERE `appointment_log`.`appointment_id` = ? AND `appointment_log`.`status` != ? LIMIT 1", [
                appointmentID,
                'cancelled'
            ], (err, appointmentLogs) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve(appointmentLogs);
            });
        });
    });
};

appointment_log.getAcceptedByUserIDAndTime = (userID, upcomingTime, currentTime) => {
    return new Promise((resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide User ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `appointment_log` JOIN `appointment` ON `appointment`.`id` = `appointment_log`.`appointment_id` WHERE `appointment`.`patients_id` = ? AND `appointment_log`.`status` = 'accepted' AND `appointment`.`start_date_stamp` <= ? AND `appointment`.`start_date_stamp` >= ? AND `appointment_log`.`status` != 'cancelled'", [
                userID,
                upcomingTime,
                currentTime
            ], (err, appointmentLogs) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve(appointmentLogs);
            });
        });
    });
};

appointment_log.getCancelledByUserIDAndTime = (userID, upcomingTime, currentTime) => {
    return new Promise((resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide User ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `appointment_log` JOIN `appointment` ON `appointment`.`id` = `appointment_log`.`appointment_id` WHERE `appointment`.`patients_id` = ? AND `appointment_log`.`status` = 'cancelled' AND `appointment`.`start_date_stamp` <= ? AND `appointment`.`start_date_stamp` >= ?", [
                userID,
                upcomingTime,
                currentTime
            ], (err, appointmentLogs) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve(appointmentLogs);
            });
        });
    });
};

appointment_log.getDelayedByUserIDAndTime = (userID, upcomingTime, currentTime) => {
    return new Promise((resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide User ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `appointment_log` JOIN `appointment` ON `appointment`.`id` = `appointment_log`.`appointment_id` WHERE `appointment`.`patients_id` = ? AND `appointment_log`.`stamp` <= ? AND `appointment_log`.`stamp` >= ? AND `appointment_log`.`status` = 'delayed' AND `appointment_log`.`status` != 'cancelled'", [
                userID,
                upcomingTime,
                currentTime
            ], (err, appointmentLogs) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve(appointmentLogs);
            });
        });
    });
};

appointment_log.updateByAppointmentID = (appointmentID, data = {}) => {
    return new Promise((resolve, reject) => {
        if (!appointmentID) {
            return reject({
                message: 'Please provide Appointment ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("UPDATE `appointment_log` SET ? WHERE `appointment_id` = ?", [
                data,
                appointmentID
            ], (err) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve();
            });
        });
    });
};

module.exports = appointment_log;