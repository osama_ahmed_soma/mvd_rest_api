const pool = require('../config/dbConnection');
const doctor_other_licenses = {};
doctor_other_licenses.getByUserID = (userID, customFields) => {
    return new Promise((resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide Doctor ID.',
                bool: false
            });
        }
        customFields = customFields || '*';
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT " + customFields + " FROM `doctor_other_licenses` JOIN `state` ON `state`.`id` = `doctor_other_licenses`.`state_id` WHERE `doctor_other_licenses`.`userid` = ?", [userID], (err, awards) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve(awards);
            });
        });
    });
};
module.exports = doctor_other_licenses;