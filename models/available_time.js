const config = require('../config/config');

const pool = require('../config/dbConnection');
const available_time = {};
available_time.getByID = id => {
    return new Promise((resolve, reject) => {
        if (!id) {
            return reject({
                message: 'Please provide Available Time ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `available_time` WHERE `id` = ?", [id], async (err, result) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                if (result.length > 0) {
                    return resolve(result[0]);
                }
                reject({
                    message: 'Available time is not exists.',
                    bool: false
                });
            });
        });
    });
};
module.exports = available_time;