const moment = require('moment-timezone');
const config = require('../config/config');

const pool = require('../config/dbConnection');
const DoctorSpecializationModel = require('./doctor_specialization');
const DoctorEducation = require('./doctor_education');
const DoctorAward = require('./doctor_award');
const DoctorOtherLicense = require('./doctor_other_licenses');
const DoctorLanguage = require('./doctor_language');
const DoctorDegree = require('./doctor_degree');
const doctor = {};
doctor.getByID = id => {
    return new Promise((resolve, reject) => {
        if (!id) {
            return reject({
                message: 'Please provide Doctor ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `doctor` WHERE `id` = ?", [id], async (err, doctor) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                if (doctor.length > 0) {
                    return resolve(doctor[0]);
                }
                reject({
                    message: 'Doctor is not exists.',
                    bool: false
                });
            });
        });
    });
};
doctor.getByUserID = (userID, customFields, isErrorHide) => {
    return new Promise((resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide Doctor ID.',
                bool: false
            });
        }
        customFields = customFields || '*';
        isErrorHide = isErrorHide || false;
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT " + customFields + " FROM `doctor` JOIN `user` ON `doctor`.`userid` =`user`.`id` WHERE `userid` = ?", [userID], async (err, doctor) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                if (isErrorHide) {
                    resolve(doctor);
                } else {
                    if (doctor.length > 0) {
                        return resolve(doctor[0]);
                    }
                    reject({
                        message: 'Doctor is not exists.',
                        bool: false
                    });
                }
            });
        });
    });
};
doctor.getByPatientID = patientID => {
    return new Promise((resolve, reject) => {
        if (!patientID) {
            return reject({
                message: 'Please provide Patient ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT `doctor`.*, `user`.`user_email` as `user_email`, `user`.`is_joined` as `is_joined` FROM `doctor` JOIN `user` ON `doctor`.`userid` = `user`.`id` WHERE `user`.`parent_id` = ? AND `user`.`type` = 'doctor'", [
                patientID
            ], async (err, doctor) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve(doctor);
            });
        });
    });
};
doctor.getSpecializationByUserID = userID => {
    return new Promise(async (resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide Doctor ID.',
                bool: false
            });
        }
        resolve(await DoctorSpecializationModel.getByDoctorID(userID));
    });
};
doctor.getEducation = userID => {
    return new Promise(async (resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide Doctor ID.',
                bool: false
            });
        }
        resolve(await DoctorEducation.getByUserID(userID));
    });
};
doctor.getAward = userID => {
    return new Promise(async (resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide Doctor ID.',
                bool: false
            });
        }
        resolve(await DoctorAward.getByUserID(userID));
    });
};
doctor.getOtherLicense = (userID) => {
    return new Promise(async (resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide Doctor ID.',
                bool: false
            });
        }
        resolve(await DoctorOtherLicense.getByUserID(userID, "`state`.`name` AS `state_name`, `state`.`id` AS `state_id`, `doctor_other_licenses`.`id` AS `doctor_other_licenses_id`"));
    });
};
doctor.getLanguage = userID => {
    return new Promise(async (resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide Doctor ID.',
                bool: false
            });
        }
        resolve(await DoctorLanguage.getByUserID(userID));
    });
};
doctor.getAvailableTime = (userID, data) => {
    return new Promise(async (resolve, reject) => {
        if (!userID) {
            return reject({
                message: 'Please provide Doctor ID.',
                bool: false
            });
        }

        // get doctor timezone
        const doctorData = await doctor.getByUserID(userID);
        const timeZone = doctorData.timezone;

        // const start = moment.tz(`${data.year}-${data.month}-${data.day} 00:00:00`, process.env.TZ);
        // const end = moment.tz(`${data.year}-${data.month}-${data.day} 23:59:59`, process.env.TZ);

        const start = moment.tz(`${data.year}-${data.month}-${data.day} 00:00:00`, timeZone);
        const end = moment.tz(`${data.year}-${data.month}-${data.day} 23:59:59`, timeZone);
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `available_time` WHERE `userid` = ? AND `month` = ? AND ( `start_date_stamp` >= ? AND `end_date_stamp` <= ? ) AND `status` = 'yes'", [
                userID,
                data.month,
                start.unix(),
                end.unix(),
            ], (err, available_time) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                if (available_time.length === 0) {
                    return reject({
                        message: 'No result found',
                        bool: false
                    });
                }
                resolve(available_time);
            });
        });
    });
};
doctor.create = (doctorID, data) => {
    return new Promise((resolve, reject) => {
        resolve();
    });
};
doctor.update = (doctorID, data) => {
    return new Promise((resolve, reject) => {
        if (!doctorID) {
            return reject({
                message: 'Please provide Doctor ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("UPDATE `doctor` SET ? WHERE `userid` = ?", [
                data,
                doctorID
            ], async (err) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        error: err,
                        bool: false
                    });
                }
                resolve();
            });
        });
    });
};
doctor.searchDoctor = params => {
    return new Promise((resolve, reject) => {
        const text = params.query || '';
        const phoneNumber = params.phone_number || '';
        const state = params.state || '';
        const specialty = params.specialty || '';
        const isOnline = params.online_now;

        const selectData = "`doctor`.`id`, `doctor`.`firstname`, `doctor`.`lastname`, `doctor`.`image_url`, `doctor`.`userid`, `doctor`.`location`, `doctor`.`stripe_one`, `doctor`.`stripe_two`, `doctor`.`online_status`";
        let whereClause = '';
        let join = "";
        let binded_arr = [];
        if (text !== '') {
            if (whereClause !== '') {
                whereClause += " AND";
            }
            whereClause += " `doctor`.`firstname` LIKE ? ESCAPE '!' OR `doctor`.`lastname` LIKE ? ESCAPE '!'";
            binded_arr.push('%' + text + '%');
            binded_arr.push('%' + text + '%');
        }
        if (state !== '') {
            if (whereClause !== '') {
                whereClause += " AND";
            }
            whereClause += " `doctor`.`state_id` = ?";
            binded_arr.push(state);
        }
        if (specialty !== '') {
            if (whereClause !== '') {
                whereClause += " AND";
            }
            join += " JOIN `doctor_specialization` ON `doctor_specialization`.`userid` = `doctor`.`userid`";
            whereClause += " `doctor_specialization`.`speciality` = ?";
            binded_arr.push(specialty);
        }
        if (phoneNumber !== '') {
            if (whereClause !== '') {
                whereClause += " AND";
            }
            whereClause += " `doctor`.`phone_home` = ? OR `doctor`.`phone_mobile` = ?";
            binded_arr.push(phoneNumber);
            binded_arr.push(phoneNumber);
        }
        if (isOnline) {
            if (whereClause !== '') {
                whereClause += " AND";
            }
            whereClause += " `doctor`.`online_status` = 'online'";
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT " + selectData + " FROM `doctor`" + join + " WHERE " + whereClause, binded_arr, async (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: err,
                        bool: false
                    });
                }
                if (results.length > 0) {
                    for (let i = 0; i < results.length; i++) {
                        results[i].specialty = await DoctorSpecializationModel.getByDoctorID(results[i].userid);
                        results[i].degree = await DoctorDegree.getByUserID(results[i].userid);
                        if (i == (results.length - 1)) {
                            // resolve here
                            resolve(results);
                        }
                    }
                } else {
                    resolve(results);
                }
            });
        });
    });
};
module.exports = doctor;
