
const pool = require('../config/dbConnection');





const appointment_knocking_data = {};

appointment_knocking_data.isDoctorAvailable = appointmentID => {
    return new Promise((resolve, reject) => {
        if (!appointmentID) {
            return reject({
                message: 'Please provide Appointment ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `appointment_knocking_data` WHERE `appointment_id` = ? AND `is_doctor_available` = 1", [
                appointmentID
            ], async (err, data) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                if(data.length > 0){
                    resolve(data);
                } else {
                    resolve(false);
                }
            });
        });
    });
};

appointment_knocking_data.updateByAppointmentID = (appointmentID, data) => {
    return new Promise((resolve, reject) => {
        if (!appointmentID) {
            return reject({
                message: 'Please provide Appointment ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("UPDATE `appointment_knocking_data` SET ? WHERE `appointment_id` = ?", [
                data,
                appointmentID
            ], async (err) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve();
            });
        });
    });
};

module.exports = appointment_knocking_data;