const moment = require('moment-timezone');
const pool = require('../config/dbConnection');

// Config
const config = require('../config/config');

const shared_files = {};

shared_files.getByFileIDAndPatientID = (fileID, patientID) => {
    return new Promise((resolve, reject) => {
        if (!fileID) {
            return reject({
                message: 'Please provide File ID.',
                bool: false
            });
        }
        if (!patientID) {
            return reject({
                message: 'Please provide Patient ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT CONCAT_WS(\" \", usr.firstname, usr.lastname) AS shared_with, f.title, sf.date_time, f.filename, sf.id FROM `shared_files` `sf` JOIN `doctor` `usr` ON `usr`.`userid` = `sf`.`shared_to_id` JOIN `files_upload` `f` ON `f`.`id` = `sf`.`file_id` WHERE `shared_by_id` = ? AND `file_id` = ?", [
                patientID,
                fileID
            ], (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve(results);
            });
        });
    });
};
shared_files.getByFileIDAndPatientIDAndDoctorID = (fileID, patientID, doctorID) => {
    return new Promise((resolve, reject) => {
        if (!fileID) {
            return reject({
                message: 'Please provide File ID.',
                bool: false
            });
        }
        if (!patientID) {
            return reject({
                message: 'Please provide Patient ID.',
                bool: false
            });
        }
        if (!doctorID) {
            return reject({
                message: 'Please provide Doctor ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("SELECT * FROM `shared_files` WHERE `file_id` = ? AND `shared_by_id` = ? AND `shared_to_id` = ?", [
                fileID,
                patientID,
                doctorID
            ], (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve(results);
            });
        });
    });
};
shared_files.insert = (fileID, doctorID, patientID) => {
    return new Promise((resolve, reject) => {
        if (!fileID) {
            return reject({
                message: 'Please provide File ID.',
                bool: false
            });
        }
        if (!doctorID) {
            return reject({
                message: 'Please provide Doctor ID.',
                bool: false
            });
        }
        if (!patientID) {
            return reject({
                message: 'Please provide Patient ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("INSERT INTO `shared_files` (`shared_to_id`, `file_id`, `shared_by_id`, `date_time`) VALUES (?, ?, ?, ?)", [
                doctorID,
                fileID,
                patientID,
                moment().tz(process.env.TZ).format('YYYY-MM-DD HH:mm:ss')
            ], (err, results) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve(results.insertId);
            });
        });
    });
};
shared_files.remove = (fileID, patientID) => {
    return new Promise((resolve, reject) => {
        if (!fileID) {
            return reject({
                message: 'Please provide Health ID.',
                bool: false
            });
        }
        if (!patientID) {
            return reject({
                message: 'Please provide Patient ID.',
                bool: false
            });
        }
        pool.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                return reject({
                    message: 'There is an error while connecting to the database. Please try again.',
                    bool: false
                });
            }
            connection.query("DELETE FROM `shared_files` WHERE `file_id` = ? AND `shared_by_id` = ?", [
                fileID,
                patientID
            ], async (err) => {
                connection.release();
                if (err) {
                    return reject({
                        message: 'There is an error while querying to the database. Please try again.',
                        bool: false
                    });
                }
                resolve();
            });
        });
    });
};

module.exports = shared_files;