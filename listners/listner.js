// Authorized
const AuthorizedMiddleWare = require('../middleware/authorized');

// Controller
const AppointmentCtrl = require('../controllers/appointment');

// Models
const DoctorModel = require('../models/doctor');

const moment = require('moment-timezone');

// Emitters
const Emitters = require('../emitters/emitter');

const onlineUsers = {};

const listner = (io) => {
    io.on('connection', async (socket) => {
        socket.userData = {};
        socket.on('checkAuth', (token) => {
            const req = {
                headers: {
                    'authorization': token
                }
            };
            const res = {
                json: (data) => {
                    return socket.emit('checkAuth', JSON.stringify(data));
                }
            };
            const next = async () => {
                // authorized
                // isSuccess = isSuccess || true;
                socket.userData = req.userData;
                // AppointmentCtrl(socket).then(() => {
                //
                // }, (error) => {
                //
                // });
                // if (!isSuccess) {
                //     delete connectedSockets[socket.userData.userid];
                //     return socket.emit('checkAuth', JSON.stringify({
                //         message: 'Token Expired.',
                //         bool: false
                //     }));
                // }
                // connectedSockets[socket.userData.userid] = socket.userData;
                return socket.emit('checkAuth', JSON.stringify({
                    message: '',
                    bool: true
                }));
            };
            AuthorizedMiddleWare(req, res, next);
        });

        socket.on('getDashboard', async (userID) => {
            Emitters(socket).dashboard(userID);
        });
        socket.on('appointment:upcoming:check', async (userID) => {
            // check for upcoming appointment and emit events if program find any upcoming appointment of this user.
            AppointmentCtrl().checkUpcoming(socket, userID);
        });
        socket.on('appointment:upcoming:knocking:doctor:available', async (appointmentID) => {
            // check for upcoming appointment and emit events if program find any upcoming appointment of this user.
            AppointmentCtrl().checkIsDoctorAvailableForKnocking(socket, appointmentID);
        });
        socket.on('appointment:upcoming:isDoctorReady', async (data) => {
            // check for upcoming appointment and emit events if program find any upcoming appointment of this user.
            data = JSON.parse(data);
            AppointmentCtrl().checkDoctorIsIn(socket, data.userID, data.appointmentID);
        });
        socket.on('checkVideoEnd', async (data) => {
            data = JSON.parse(data);
            AppointmentCtrl().checkVideoEnd(socket, data.userID, data.appointmentID);
        });

        socket.on('onlineNow', userID => {
            onlineUsers[socket.id] = userID;
            DoctorModel.update(userID, {
                online_status: 'online'
            });
        });

        socket.on('disconnect', () => {
            DoctorModel.update(onlineUsers[socket.id], {
                online_status: 'offline'
            });
            delete onlineUsers[socket.id];
        });

    });
};
module.exports = listner;