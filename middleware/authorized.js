const jwt = require('jsonwebtoken');
const config = require('../config/config');

// Models
const PatientModel = require('../models/patient');

module.exports = (req, res, next) => {
    try {
        const token = req.headers['authorization'];
        if (!token) {
            return res.json({
                message: 'No token provided.',
                bool: false
            });
        }
        jwt.verify(token, config.secret, async (err, decoded) => {
            if (err) {
                return res.json({
                    message: 'Failed to authenticate token.',
                    bool: false
                });
            }
            const patientData = await PatientModel.getByUserID(decoded.userid);
            const currentDateObj = new Date();
            if (currentDateObj.getTime() > decoded.expiresIn) {
                return res.json({
                    message: 'Token Expired.',
                    bool: false
                });
            }
            // if everything is good, save to request for use in other routes
            req.userData = decoded;
            delete req.userData.userpassword;
            if (req.userData.image) {
                req.userData.image = config.originalServer + patientData.image_url;
            }
            // set user timezone
            // process.env.TZ = req.userData.user_timezone;
            if (req.userData.user_timezone !== patientData.timezone) {
                process.env.TZ = patientData.timezone;
            } else {
                process.env.TZ = req.userData.user_timezone;
            }
            next();
        });
    } catch (e) {
        return res.json(e);
    }
};