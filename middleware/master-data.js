const Message = require('../models/message');
const Notification = require('../models/notification');
module.exports = async (req, res, next) => {
    try {
        const UnreadMessagesCount = await Message.getUnreadCount(req.userData.userid);
        const UnreadNotifications = await Notification.getUnreadCount(req.userData.userid);
        req.unread_messages = UnreadMessagesCount;
        req.unread_notifications = UnreadNotifications;
        next();
    } catch (e) {
        res.json(e);
    }
};