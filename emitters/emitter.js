// Config
const config = require('../config/config');

// Models
const StateModel = require('../models/state');
const SpecialtyModel = require('../models/specialty');
const MessageModel = require('../models/message');
const AppointmentModel = require('../models/appointment');
const NotificationModel = require('../models/notification');
const FavoriteModel = require('../models/favorite');
const FilesUploadModel = require('../models/files_upload');
const PatientModel = require('../models/patient');

// Controller
const ProfileCompletionController = require('../controllers/profile-completion');

const emitter = {};
const calcTodayUpcomingAppointment = appointments => {
    return new Promise((resolve) => {
        let totalCount = 0;
        for(let i = 0; i < appointments.length; i++){
            if(appointments[i].start.secondLeft <= 86400){
                totalCount++;
            }
            if(i == (appointments.length - 1)) {
                resolve(totalCount);
            }
        }
    });
};
emitter.dashboard = async (userID) => {
    let result = {};
    try {
        emitter.socket.userData.userid = userID;
        const profileCompletionData = await ProfileCompletionController.getProfileCompletion(emitter.socket.userData.userid);
        const states = await StateModel.getAll();
        const specialties = await SpecialtyModel.getAll();
        // get number of inbox messages
        const totalInbox = await MessageModel.getInbox(emitter.socket.userData.userid);

        const totalUpcomingAppointment = await AppointmentModel.getUpcoming(emitter.socket.userData.userid);

        const totalAppointment = await AppointmentModel.getAll(emitter.socket.userData.userid);
        // get total upcoming appointment

        const UnreadMessagesCount = await MessageModel.getUnreadCount(emitter.socket.userData.userid);
        const UnreadNotificationsCount = await NotificationModel.getUnreadCount(emitter.socket.userData.userid);
        const UnreadNotifications = await NotificationModel.getAll(emitter.socket.userData.userid);

        // get Total my physicians
        const totalMyPhysician = await FavoriteModel.getMyDoctors(emitter.socket.userData.userid);

        // get total my files
        const files = await FilesUploadModel.getAll(emitter.socket.userData.userid);

        // check for state_id
        const patientData = await PatientModel.getByUserID(emitter.socket.userData.userid);
        emitter.socket.userData.state_id = patientData.state_id;
        emitter.socket.userData.image = config.originalServer + patientData.image_url;

        // today upcoming appointment total
        let todayUpcomingAppointmentTotal = 0;
        if (totalUpcomingAppointment.length > 0) {
            todayUpcomingAppointmentTotal = await calcTodayUpcomingAppointment(totalUpcomingAppointment);
        }

        result = {
            message: {
                inbox: {
                    unread: UnreadMessagesCount,
                    total: totalInbox.length
                }
            },
            appointment: {
                upcoming: totalUpcomingAppointment.length,
                upcomingAppointmentData: totalUpcomingAppointment,
                total: totalAppointment.length,
                upcomingAppointmentNewData: {
                    total: totalUpcomingAppointment.length,
                    todayTotal: todayUpcomingAppointmentTotal
                }
            },
            notifications: {
                unread: {
                    count: UnreadNotificationsCount,
                    data: UnreadNotifications
                }
            },
            userData: emitter.socket.userData,
            profileCompletionData: profileCompletionData,
            states: states,
            specialties: specialties,
            myPhysician: {
                total: totalMyPhysician.length
            },
            files: {
                total: files.length
            },
            bool: true
        };
        emitter.socket.emit('dashboard', JSON.stringify(result));
    } catch (e) {
        result = e;
        emitter.socket.emit('dashboard', JSON.stringify(result));
    }
};

emitter.appointmentFound = (object) => {
    emitter.socket.emit('appointment:upcoming:checked', JSON.stringify(object));
};
emitter.appointmentIsDoctorAvailableForKnocking = (object) => {
    emitter.socket.emit('appointment:upcoming:knocking:doctor:available', JSON.stringify(object));
};
emitter.appointmentDoctorIsReady = () => {
    emitter.socket.emit('appointment:upcoming:doctorIsReady');
};
emitter.appointmentDoctorEndConsultation = () => {
    emitter.socket.emit('appointment:doctorEndConsultation', '');
};
module.exports = socket => {
    emitter.socket = socket;
    return emitter;
};